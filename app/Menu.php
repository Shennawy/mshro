<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menus';
    protected $fillable = ['parent_id', 'title', 'link', 'target', 'order_by', 'status', 'type'];
    public $timestamps = false;

}
