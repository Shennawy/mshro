<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecificationPost extends Model 
{

    protected $table = 'specification_post';
    public $timestamps = true;
    protected $fillable = array('post_id', 'specification_id', 'value');

}