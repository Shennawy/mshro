<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    //
    public function Participants(){
      return $this->hasMany(Participant::class);
    }
    protected $appends = ['partCount'];
    function getPartCountAttribute(){
      return $this->Participants()->count();
    }
    public function Messages(){
      return $this->hasMany(Message::class);
    }
    public function ParticipantStatus($user){
      return Participant::where('user_id',$user->id)->where('thread_id',$this->id)->first()->status;
    }


    public function Files(){
      $user = Auth::user();

      $p = $user->getPartInThreadObj($this);

      $items = $this->Items()->where('type','NOT LIKE','image'.'%')->where('type','NOT LIKE','video'.'%')->latest()->get();
      if($p->status == 3){
        $items = $items->where('created_at','<=',$p->updated_at);
      }
      return $items;
    }

    function getLatestMsg(){
      $user = Auth::user();
      $messages = $this->Messages()->latest();

      if($user->hasLeftThread($this)){
        $p = $user->getPartInThreadObj($this);
        $messages = $messages->where('created_at','<=',$p->updated_at)->first();
        if($messages->type == 2){
          $info = json_decode($messages->body);
          if($info->type == Message::LEAVE){
            $icon = 'zmdi-mail-reply';
            $text = ($messages->User->name.' Left The Group');
            $messages->body = $text;
          }
        }
        return $messages;
      }
      $messages = $messages->first();

      if($messages && $messages->type == 2){
        $info = json_decode($messages->body);
        if($info->type == Message::LEAVE){
          $icon = 'zmdi-mail-reply';
          $text = ($messages->User->name.' Left The Group');
          $messages->body = $text;
        } else if($info->type == Message::ADDED){
          $icon = 'zmdi-accounts-add';
          $text = ($messages->User->name.' Added '.$info->user->name .' To The Group');
          $messages->body = $text;

        } else if($info->type == Message::CHANGED_COLOR){
          $icon = 'zmdi-alert-circle';
          $text = ($messages->User->name.' Changed The Color Of The Chat');
          $messages->body = $text;

        } else if($info->type == Message::FRIENDS){
          $icon = 'zmdi-accounts';
          $text = 'You Became Friends';
          $messages->body = $text;

        }
      }
      return $messages;
    }
}
