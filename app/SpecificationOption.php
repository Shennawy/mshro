<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecificationOption extends Model 
{

    protected $table = 'specification_option';
    public $timestamps = true;
    protected $fillable = array('specification_id', 'name');

    public function Spaci()
    {
        return $this->belongsTo('App\Specification', 'specification_id');
    }

}