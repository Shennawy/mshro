<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasFiles;
class Blog extends Model
{
    use HasFiles;
    protected $table = 'blogs';
    public $timestamps = true;
    protected $fillable = ['user_id','title', 'image', 'body'];
    // LogsActivityData
    protected static $logName = 'blogModel';
    protected static $logAttributes = ['العنوان'=>'title','النص'=> 'body'];

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}
