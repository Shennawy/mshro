<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostExclusive extends Model 
{

    protected $table = 'post_exclusives';
    public $timestamps = true;
    protected $fillable = array('post_id', 'user_id');

}