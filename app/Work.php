<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{

    protected $table = 'works';
    public $timestamps = true;
    protected $fillable = array('user_id', 'title', 'desc', 'image', 'url', 'tags');

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    function getImageAttribute($image){
      return $image ?? url('front/images/main/pages_header.png');
    }
}
