<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model 
{

    protected $table = 'categories';
    public $timestamps = true;
    protected $fillable = array('name', 'parent_id');

    public function Posts()
    {
        return $this->hasMany('App\Post', 'cat_id');
    }

    public function Spacis()
    {
        return $this->hasMany('App\Specification', 'cat_id');
    }

}