<?php

namespace App;

use App\Notifications\AdminResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\Permission\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\CanUploadFiles;

class Admin extends Authenticatable
{
    use Notifiable,
        HasRoles,
        SoftDeletes,
        CanUploadFiles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded=['admin'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }

}
