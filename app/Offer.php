<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{

    protected $table = 'offers';
    public $timestamps = true;
    protected $fillable = array('user_id', 'body', 'price', 'duration', 'status','post_id');

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function Post()
    {
        return $this->belongsTo('App\Post', 'post_id');
    }

}
