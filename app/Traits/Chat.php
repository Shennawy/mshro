<?php namespace App\Traits;
use Auth;
use Carbon\Carbon;
use App\Participant;
use App\Message;
use stdClass;
use App\Notifications\MessageReceived;

use App\User;
use App\Thread;
trait Chat
{
  function getThreadTarget($thread,$me = 0){
    if(!$thread->is_group){
      $part = Participant::where('thread_id',$thread->id);
      if(!$me)
      $part = $part->whereNotIn('user_id',[$this->id])->first();
      else
      $part = $part->first();

      if($part)
      return User::find($part->user_id);
    }
  }
  public function Participants(){
    return $this->hasMany(Participant::class);
  }
  public function Threads(){
    return $this->belongsToMany(Thread::class,'participants','thread_id','user_id');
  }
  function unreadMessagesCount(){
    $user = $this;
    $notifications = $user->unreadNotifications()->whereNull('read_at')->get();
    $msgsCount = 0;
    $thread_ids = [];
    foreach ($notifications as $notification) {

          if($notification->type == 'App\Notifications\MessageReceived'){

            $th_id = $notification->data['msg']['thread_id'];
            if(!in_array($th_id, $thread_ids)){
            $p = Participant::where('thread_id',$th_id)
                                    ->where('user_id',$user->id)->first();
            if($p){

              $msgsCount++;
            }

          }
          }


        }
        return $msgsCount;
  }
  function getMessagesWithUser($user){
    $thread = $this->getThreadIdWith($user);
    return Message::where('thread_id',$thread)->get();
  }
  public function messages()
    {
        return $this->hasMany(Message::class);
    }


  public function isPartInThread($thread){
    if($thread && Participant::where('thread_id',$thread->id)->where('user_id',$this->id)->count()){
      return 1;
    }
    return 0;
  }
  function getUserByThread($thread){
    if($thread){
      if($this->isPartInThread($thread) && !$thread->is_group){
        if($thread->partCount == 1)
        return User::find($thread->Participants()->first()->user_id);
        else
        return User::find($thread->Participants()->where('user_id','<>',$this->id)->first()->user_id);
      }

    }
    return 0;

  }



  public function getThreadIdWith($user){
    if(!$user){
      $user = $this;
    }
    if($user->id != $this->id){
    $userTheads = $this->Participants()->pluck('thread_id')->toArray();
    $userTheads = Thread::whereIn('id',$userTheads)->where('is_group',0)->pluck('id')->toArray();
    $userParticipants = Participant::whereIn('thread_id',$userTheads)->where('user_id',$user->id)->first();
    if($userParticipants)
    return $userParticipants->thread_id;
} else {
  $userTheads = $this->Participants()->pluck('thread_id')->toArray();
  $userTheads = Thread::whereIn('id',$userTheads)->where('is_group',0)->get();
  return $userTheads->where('partCount',1)->first() ? $userTheads->where('partCount',1)->first()->id : 0;
}
    return 0;
  }
  function getChatPeople(){
    $user = Auth::user();
    $thread_ids = $user->Participants()->where('status',1)->pluck('thread_id')->toArray();
    $thread_ids = Thread::whereIn('id',$thread_ids)->where('is_group',0)->latest('updated_at')->pluck('id')->toArray();
    $ids_ordered =  implode(',', $thread_ids);
    $otherUsers = Participant::whereIn('thread_id',$thread_ids)->whereNotIn('user_id',[$user->id])
    // ->orderByRaw(\DB::raw("FIELD(thread_id, $ids_ordered)"))
    ->pluck('user_id')->toArray();
    $ids_ordered =  implode(',', $otherUsers);

    $others = User::whereIn('id',$otherUsers)
    // ->orderByRaw(\DB::raw("FIELD(id, $ids_ordered)"))
    ->get();

    return $others;

  }
  public function sendToUser($recipant,$body,$images){
      $thread = Thread::find($this->getThreadIdWith($recipant));

      if($thread){
        return $this->sendToThread($thread,$body,$images);
      } else {
        $thread = new Thread();
        $thread->latest_used =  Carbon::now()->toDateTimeString();
        $thread->is_group = 0;
        $thread->save();
        $participant = new Participant();
        $participant->user_id = Auth::user()->id;
        $participant->status = 1;
        $participant->thread_id = $thread->id;
        $participant->save();
        if($this->id != $recipant->id){
        $participant = new Participant();
        $participant->user_id = $recipant->id;
        $participant->status = 1;

        $participant->thread_id = $thread->id;
        $participant->save();
      }


        return $this->sendToThread($thread,$body,$images);
      }
      $thread_ids =  Participant::where('user_id',Auth::user()->id)->groupBy('thread_id')->pluck('thread_id')->toArray();
      // $thread_ids_arr = [];
      // // $thread_ids_arr = $thread_ids->pluck('thread_id');
      // // dd($thread_ids_arr);
      // foreach ($thread_ids as $t) {
      //   array_push($thread_ids_arr, $t->thread_id);
      // }
      $thread = 0;
      // $threads = Thread::where('is_group',0)->whereIn('id', $thread_ids)->get();
      // foreach ($threads as $t) {
        $participant = Participant::where('user_id',$recipant->id)->whereIn('thread_id',$thread_ids)->first();
        if($participant){
          $thread = Thread::find($participant->thread_id);
          //break;
        }
    //  }
     {
      $new = 1;
      $thread = new Thread();



      $message = new Message();
      $message->thread_id = $thread->id;
      $message->user_id = Auth::user()->id;
      $message->body = $request['body'];
      $thread = Thread::where('id',$message->thread_id)->first();
      $thread->latest_used =  Carbon::now()->toDateTimeString();
      $thread->save();

      $message->save();

      $recipant->notify(new MessageReceived($message));

      if($new){
        $target = $recipant;
        $user = Auth::user();
      }
      $msgs = [$message];

      $html = view('chat.single-msg',compact(['msg']))->render();
      $t = $user;
      $user = $target;
      $target = $t;

      return response()->json(array('success' => true,
                                  'msg'=>$message->body,'msg_id'=>$message->id,'user_id'=>Auth::user()->id,'html'=>$html));
    }
  }
  public function getPartInThreadObj($thread){
    if($thread)
    return Participant::where('thread_id',$thread->id)->where('user_id',$this->id)->first();
    return 0;

  }
  public function sendToThread($thread,$body,$images,$type = 0,$returnMsg = 0){
      $obj = $this->getPartInThreadObj($thread);
      if($obj->status != 4){
        $message = new Message();
        $message->thread_id = $thread->id;
        $message->user_id = Auth::user()->id;
        $message->type = $type;
        $message->body = $body;
        $thread = Thread::where('id',$message->thread_id)->first();
        $thread->latest_used =  Carbon::now()->toDateTimeString();
        $thread->save();
        $obj->status = 1;
        $obj->save();

        $message->save();
        if($images){
          $images = Auth::user()->generalUploadFiles($images);


        }
        $p = Participant::where('user_id',Auth::user()->id)->where('thread_id',$thread->id)->first();
        $p->status = 1;
        $p->save();
        $recipant = 0;
         {
          foreach ($thread->Participants()->where('user_id','!=',Auth::user()->id)->get() as $p) {
            $recipant = $p->User;

              $recipant->notify(new MessageReceived($message));
              $target = $recipant;


          }
        }
        $target = $recipant;



        $user = Auth::user();
        $msg = $message;
        if(!$target){
          $target = $user;
        }
        $html = view('chat.single-msg',compact(['msg']))->render();
        $receiverHtml = '';
        if($user->id != $target->id){

        $t = $user;
        $user = $target;
        $target = $t;
        $receiver = 1;
        $msg = $message;

        $receiverHtml = view('chat.single-msg',compact(['msg']))->render();
      }
        if(!$returnMsg)
        return response()->json(array('success' => true,
                                    'msg'=>$message->body,
                                    'msg_id'=>$message->id,
                                    'html'=>$html,'receiverHtml'=>$receiverHtml,'thread'=>$thread));

                                    return $message;
                                  } else {
                                    return response()->json(array('success'=>false,'title'=>'something wrong happened','msg'=>'You aren\'t in this thread'));

                                  }
    }

}
