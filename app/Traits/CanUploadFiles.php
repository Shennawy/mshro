<?php namespace App\Traits;

use Auth;
use Carbon\Carbon;

use stdClass;
use App\File;
use App\ModelFile;
use Illuminate\Database\Eloquent\Collection;
trait CanUploadFiles
{
  function saveFiles($files){
    if(!is_array($files)){
      $files = [$files];
    }
    $allFiles = [];
    foreach ($files as $file) {
      $file_name = $this->id . md5((string)mt_rand(1000000,10000000)) .'.'. $file->getClientOriginalExtension();
                $path = 'public/images';
                $fileSize = ($file->getClientSize()/1024);
                $mime = $file->getClientMimeType();

                $file->move('uploadedimages/',$file_name);
                  $fileObj = new File();
                  $fileObj->owner_id = $this->id;
                  $fileObj->owner_type = get_class($this);
                  $fileObj->name = $file->getClientOriginalName();
                  $fileObj->extension = $file->getClientOriginalExtension();
                  $fileObj->local_path = public_path().'/uploadedimages/'.$file_name;
                  $fileObj->mime = $mime;
                  $fileObj->url = '/uploadedimages/'.$file_name;
                  $fileObj->file_size = $fileSize;
                  $fileObj->save();
                  array_push($allFiles, $fileObj);
    }
    return $allFiles;
  }
  function generalUploadFiles($files){
    return $this->saveFiles($files);
  }
  function uploadedFiles(){
    return $this->morphMany(File::class,'owner');
  }

}
