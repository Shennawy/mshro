<?php namespace App\Traits;

use Auth;
use Carbon\Carbon;

use stdClass;
use App\File;
use App\ModelFile;
use Illuminate\Database\Eloquent\Collection;
trait HasFiles
{
  function Files(){
      return $this->morphToMany(File::class,'model','model_files');
  }
  function ModelFiles(){
    return $this->morphMany(ModelFiles::class,'model');
  }
  function AttachFiles($files){
    $files = [];
    foreach ($files as $file) {
      $file = ModelFile::save($this,$file);
      $files[] = $file;
    }
    return $files;
  }

  function deAttachFiles($files){
    if($files instanceof File)
      $ids = [$files->id];
    if($files instanceof Collection){
      $ids = $files->pluck('id')->toArray();
    }
    return $this->ModelFiles()->whereIn('file_id',$ids)->delete();
  }
}
