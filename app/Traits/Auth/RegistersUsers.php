<?php

namespace App\Traits\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Auth\Events\Registered;
use abdullahobaid\mobilywslaraval\Mobily;

use App\User;

trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        //role assign
        $user->assignRole(1);

        $this->guard()->login($user);

        Session()->flash('flash_message','مرحبا بك '.$user->name.' فى '.getSettings());

        return $this->registered($request, $user) ?: redirect($this->redirectPath())->with('user',$user);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $r, $user)
    {
        
        // $msg = Mobily::send($user->phone, 'أضغط للتسجيل فى '.getSettings().' '.$r->root().'/complete/'.$user->phone.$user->active.' هذا الرابط خاص وسرى يجب عدم إرساله لأى شخص - تنتهى صلاحية هذا الرابط خلال '.getSettings('delFailedUsers').' أيام من الآن');
        // if($msg === true){
        //     Session()->flash('successSendActiveCode','تم إرسال بيانات التسجل لجوالك');
        //     return back();
        // }else{
        //     User::where('id',$user->id)->first()->forceDelete();
        //     Session()->flash('error_flash_message',$msg);
        //     return back();
        // }

    }


}
