<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Mail;
use App\Contact;

class ContactController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:contact-list', ['only' => ['index', 'show']]);
    }

    public function index()
    {
        $contacts = Contact::orderBy('id', 'desc')->orderBy('active', 'asc')->get();
        $num = 1;
        return view('admin.contacts.index', compact('contacts', 'num'));
    }

    public function show($id)
    {

        $contact = Contact::find($id);
        Contact::where('id', $id)->update(['active' => 0]);
        return view('admin.contacts.viewMsg', compact('contact'));
    }

    public function replay(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'body' => 'required',
        ], [
            'email.required' => 'البريد الاكترونى مطلوب',
            'body.required' => 'أكتب النص مطلوب',
        ]);

        Mail::send('mails.contactReplay', ['request' => $request], function ($m) use ($request) {
            $m->to($request->email)->subject(getSettings('SiteName'));
        });

        return back()->withFlashMessage('تم الارسال بنجاح');

    }

    public function create()
    {
        return view('front.contact.add');
    }

    public function store(Request $request, Contact $contact)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'subject' => 'required',
            'body' => 'required',
        ], [
            'name.required' => 'الإسم مطلوب',
            'email.required' => 'البريد الاكترونى مطلوب',
            'phone.required' => 'رقم الهاتف مطلوب',
            'subject.required' => 'أكتب عنوان الموضوع',
            'body.required' => 'أكتب النص مطلوب',
        ]);

        Auth()->check() ? $user = Auth()->user()->id : $user = NULL;

        $newContact = Contact::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'type' => $request->type ? $request->type : 1,
            'subject' => $request->subject,
            'body' => $request->body,
            'active' => 1,
            'user_id' => $user,
        ]);
        return back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'تـم الإرسال بنجاح : رقم رسالتك - ' . $newContact->id]));
    }

    function destroy($id)
    {
        $msg = Contact::find($id)->delete();
        if (!$msg) {
            return "error";
        }
        if (Contact::count()) {
            return "done";
        } else {
            return "empty";
        }
    }
}
