<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    //
    function getPost($id){
      $post = \App\Post::findOrFail($id);
      return view('admin.posts.single-post',compact('post'));
    }
function getPosts(){
  $posts = \App\Post::latest()->get();
  if(request('status') !== null){
    $posts = $posts->where('status',request('status'));
  }
  return view('admin.posts.index',compact('posts'));
}
function activatePost($id){
  $post = \App\Post::findOrFail($id);
  $post->status = 1;
  $post->save();
  return redirect()->back();
}
function savePost(){
  $post = new \App\Post();
  if(request('id')){
    $post = Post::findOrFail(request('id'));
    if($post->user_id != Auth::user()->id)
      $post = Post::findOrFail('undefined');
  }
  $data = request()->except(['_token']);
  $cat = \App\Category::find(1);
  if(!$cat){
    $cat =  new \App\Category();
    $cat->name = "";
    $cat->save();
  }
  $data['cat_id'] = 1;
  $post->fill($data);
  $post->save();
  return redirect('/admincp/posts');
}
}
