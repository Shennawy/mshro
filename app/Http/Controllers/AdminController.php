<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\User;
use App\Department;
use DB;
use Hash;
use Validator;

class AdminController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:admin-list', ['only' => ['index','show']]);
        $this->middleware('permission:admin-create', ['only' => ['create','store']]);
        $this->middleware('permission:admin-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:admin-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Admin::all();
        return view('admin.admins.index',compact('users'));
    }

    public function show ($type){
        switch ($type) {
            case 'archive-users':
                $users = Admin::onlyTrashed()->get();
                break;
            default:
                return back()->withFlashMessage(json_encode(['success'=>false,'msg'=>'عذرا لقد طلبت صفحة غير موجوده']));
                break;
        }
        return view('admin.admins.index',compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Admin::find($id);
        if(!$user){
            $user = Admin::onlyTrashed()->findOrFail($id);
        }
        $userRole = $user->roles->pluck('name')->all();
        $roles = Role::pluck('slug','name')->all();
        return view('admin.admins.edit',compact('user','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Admin::find($id);
        if(!$user){
            $user = Admin::onlyTrashed()->findOrFail($id);
        }

        $data = $request->validate([
            'name'              => 'required|max:255',
            "email"             => 'required|email|max:255|unique:admins,email,' .$user->id,
        ],[
            'name.required'     => 'الإسم مطلوب',
            "email.required"    => 'البردي الإبكترونى مطلوب',
        ]);

        $password = $request->validate([
            'password'          => request('password') ? 'min:6|confirmed' : '',
        ]);

        if(request('curPassword')){
            if(Hash::check(request('curPassword'), $user->password)){
                $user->update(['password' => bcrypt(request('password'))]);
            }else{
                return back()->withErrors(['curPassword'=>'كلمة المرور القديمة غير صحيحه']);
            }
        }

        $user->update($data);
        $user->syncRoles(request('roles'));

        return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل البيانات بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth()->user()->id == $id){
            return response()->json(['success'=>false,'msg'=>'عذرا لا يمكنك حذف الحساب الخاص بك']);
        }
        Admin::findOrFail($id)->delete();
        return response()->json(['success'=>true,'msg'=>'تم نقل المشرف بنجاح']);
    }

    public function restore($id){
        Admin::onlyTrashed()->findOrFail($id)->restore();
        return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم إسترجاع المشرف بنجاح']));
    }

    public function force($id){
        if(Auth()->user()->id == $id){
            return back()->withFlashMessage(json_encode(['success'=>false,'msg'=>'عذرا لا يمكنك حذف الحساب الخاص بك']));
        }

        Admin::onlyTrashed()->findOrFail($id)->forceDelete();
        DB::table("model_has_roles")->where('model_type','App\Admin')->where('model_id',$id)->delete();
        return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف المشرف بشكل نهائى']));
    }

    public function changePassword (Request $request)
    {
        if(request()->isMethod('get')){
            return view('admin.admins.changePassword');
        }else{
            $data = $request->validate([
                'password'              => 'required|min:6|confirmed',
            ],[
                'password.required'     => 'أكتب كلمة المرور',
            ]);
            $user = \Auth::user();
            if(request('curPassword')){
                if(Hash::check(request('curPassword'), $user->password)){
                    $user->update(['password' => bcrypt(request('password'))]);
                }else{
                    return back()->withErrors(['curPassword'=>'كلمة المرور القديمة غير صحيحه']);
                }
            }
            return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل كلمة المرور بنجاح']));
        }
    }
}
