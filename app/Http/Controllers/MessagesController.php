<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Thread;
use App\Participant;
class MessagesController extends Controller
{
    function getChatPage($id = 0){
      return view('chat.index',compact('id'));
    }
    public function getChatMsgs(Request $request)
    {
        $target = User::where('id', $request['user_id'])->first();
        if ($request['recipant'])
            $target = User::where('id', $request['recipant'])->first();

        $user = Auth::user();
        $thread = 0;
        $left = 0;

        if ($request['thread_id']) {
            $thread = Thread::find($request['thread_id']);
            if (!$target && !$thread->is_group) {
                $target = $user->getUserByThread($thread);
            }
        } else {
            $thread_ids = [];
            $targetThreads = [];

            $thread_ids = Participant::where('user_id', Auth::user()->id)->pluck('thread_id')->toArray();
            $targetThreads = Participant::where('user_id', $target->id)->pluck('thread_id')->toArray();

            $ids = array_intersect($thread_ids, $targetThreads);
            if (Auth::user()->id == $target->id)
                $thread = Thread::whereIn('id', $ids)->where('is_group', 0)->get()->where('partCount', 1)->first();
            else
                $thread = Thread::whereIn('id', $ids)->where('is_group', 0)->first();
            // foreach ($thread_ids as $t) {
            //   array_push($thread_ids_arr, $t->thread_id);
            // }
            // $thread = 0;
            // $threads = Thread::where('is_group',0)->whereIn('id', $thread_ids_arr)->get();
            // foreach ($threads as $t) {
            //   if(Participant::where('user_id',$target->id)->first()){
            //     $thread = Thread::find($t->id);
            //     break;
            //   }
            // }
        }
        $msgs = [];
        if ($thread) {
            $deleted = $user->deletedMsgs($thread)->pluck('msg_id');
            $partinobj = $user->getPartInThreadObj($thread);
            if ($partinobj && $partinobj->status == 3) {
                $msgs = $thread->Messages()->whereNotIn('id', $deleted)->where('created_at', '<', $partinobj->updated_at)->latest()->get();

            } else {
                $msgs = $thread->Messages()->whereNotIn('id', $deleted)->latest()->get();
                if ($user->hasLeftThread($thread)) {
                    $p = $user->getPartInThreadObj($thread);
                    $msgs = $msgs->where('created_at', '<=', $p->updated_at);
                }
            }
            //dd($msgs);
            //print_r($msgs);
            $msgs = Helper::paginate($msgs, 15);
        }
        if (!$thread) {
            $thread = new stdClass();
            $thread->id = 0;
            $thread->is_group = 0;

        }
        $html = '';

//  print_r($msgs);
        $first_meet = 0;
        if (!$target && !$thread->is_group) {
            $target = User::where('username', $request['username'])->first();
        }
        $settings = 0;


        if (!$thread->is_group) {
            if (!count($msgs) || (count($msgs) && $msgs->currentPage() == $msgs->lastPage()) || !$thread->id) {
                $first_meet = view('components.first-meet', compact('target'))->render();

            }
        } else {
            $left = $user->getPartInThreadObj($thread)->status == 3 ? 1 : 0;
            if (!$left) {
                $user->seeThread($thread);
            }
        }
        {
            $settings = new stdClass();
            $color = ChatSettings::where('thread_id', $thread->id)->where('name', 'color')->latest()->first();
            if (!$color) {
                $color = ChatColor::first();
                $settings->color = new stdClass();
                $settings->color->value = $color;
            } else {
                $settings->color = $color;
            }
        }
//  dd($msgs);
        $html = view('components.new-chat-component', compact(['user', 'target', 'thread', 'msgs']))->render();
        $block = 0;
        $blockrel = 0;
        $deactivated = 0;

        $chatSettingHtml = '';


        $user->seeThread($thread);
        if ($request['user_id']) {
            $block = $user->blockedMessages($target);

            $blockrel = $user->hasBlockMessageRelation($target);
            if (!$blockrel) {
                $blockrel = !$user->canSendMessage($target->id);
            }
            $deactivated = !$target->activated;
        } else {
            $target = $user->getThreadTarget($thread);
            if ($target) {
                $block = $user->blockedMessages($target);
                $blockrel = $user->hasBlockMessageRelation($target);
                if (!$blockrel) {
                    $blockrel = !$user->canSendMessage($target->id);
                }
                $deactivated = !$target->activated;
            }
        }
        $smallHtml = view('components.small-chat-info', compact(['thread', 'target']))->render();
        if ($thread) {

            $chatSettingHtml = view('components.new-chat-settings', compact(['thread', 'target']))->render();
        }
        return response()->json(['html' => $html, 'left' => $left, 'small_info' => $smallHtml, 'settngsHtml' => $chatSettingHtml, 'deactivated' => $deactivated, 'block_rel' => $blockrel, 'user_block' => $block, 'first_meet' => $first_meet, 'settings' => $settings, 'msgs' => $msgs, 'thread' => $thread]);
    }

    public function getChatBoxComponent(Request $request)
    {
        $target = User::where('id', $request['user_id'])->first();
        $user = Auth::user();
        $thread = 0;
        if ($request['thread_id']) {
            $thread = Thread::find($request['thread_id']);
        } else {
            $thread_ids = [];
            $targetThreads = [];

            $thread_ids = Participant::where('user_id', Auth::user()->id)->pluck('thread_id')->toArray();
            $targetThreads = Participant::where('user_id', $target->id)->pluck('thread_id')->toArray();

            $ids = array_intersect($thread_ids, $targetThreads);

            $thread = Thread::whereIn('id', $ids)->where('is_group', 0)->first();
            // foreach ($thread_ids as $t) {
            //   array_push($thread_ids_arr, $t->thread_id);
            // }
            // $thread = 0;
            // $threads = Thread::where('is_group',0)->whereIn('id', $thread_ids_arr)->get();
            // foreach ($threads as $t) {
            //   if(Participant::where('user_id',$target->id)->first()){
            //     $thread = Thread::find($t->id);
            //     break;
            //   }
            // }
        }
        if (!$thread) {
            $thread = new stdClass();
            $thread->id = 0;
        }

        $html = view('components.chat-box', compact(['user', 'target', 'thread']))->render();
        return response()->json(array('success' => true,
            'html' => $html));
    }

    public function send(Request $request)
    {
        $user = Auth::user();
        $recipant = $request['recipant'] ? User::find($request['recipant']) : 0;
        $new = 0;
        $html = '';
        $thread = Thread::find($request['thread_id']);
        if ($thread) {
            $target = $user->getThreadTarget($thread, $recipant && ($recipant->id == $user->id));
            if ($target) {
                return $user->sendToThread($thread, $request['body'], $request['images']);
            } else {
                return response()->json(array('success' => false, 'title' => 'something wrong happened', 'msg' => 'You Can\'t Send Messages To This User'));

            }
        }


        if ($recipant) {
            return $user->sendToUser($recipant, $request['body'], $request['files']);

        }

    }

    public function seeThread(Request $request)
    {
        $thread = Thread::find($request['id']);
        $user = Auth::user();
        if ($thread)
            $user->seeThread($thread);

        return response()->json(['success' => true]);
    }

    public function getAllDeliverd(Request $request)
    {
        $user = Auth::user();
//  $user->getAllDelivered();
        $threadsToDeliver = $user->getAllDeliverd();

        return response()->json(['success' => true, 'delivered_threads' => $threadsToDeliver]);

    }

    public function acceptThread(Request $request)
    {
        $user = Auth::user();

        $p = Participant::where('user_id', $user->id)->where('thread_id', $request['thread_id'])->first();
        $p->status = 1;
        $p->save();
        $u = User::find($request['user_id']);
        $notMsg = $user->name . ' Accepted Your Messenger Request';
//$notification = ;

        $u->notify(new Normal($notMsg, 'messenger', '', $user, $user));
// $notification->about_id = $request['thread_id'];
// $notification->about_type = 'App\Thread';
// $notification->save();
        return response()->json(array('success' => true));
    }

    public function ignoreThread(Request $request)
    {
        $user = Auth::user();

        $p = Participant::where('user_id', $user->id)->where('thread_id', $request['thread_id'])->first();
        $p->status = 3;
        $p->save();
// $u = User::find($request['user_id']);
// $notMsg = $user->name . ' Accepted Your Messenger Request';
// $notification = new Normal($notMsg,'messenger','',$user,$user);
//
// $u->notify($notification);
// $notification->about_id = $request['thread_id'];
// $notification->about_type = 'App\Thread';
// $notification->save();
        return response()->json(array('success' => true));
    }


    public function unsendMessage(Request $request)
    {
        $msg = Message::find($request['id']);
        $user = Auth::user();

        if ($msg && $user->id == $msg->user_id) {
            $id = $msg->id;
            $thread = $msg->Thread;
            // $toBroadcast = array_column($thread->Participants(),'id');
            if ($msg->delete()) {

                return response()->json(array('success' => true,
                    'msg_id' => $id,
                    'thread' => $thread,
                ));
            }
        }
        return response()->json(array('success' => false, 'title' => 'something wrong happened', 'msg' => 'You Can\'t Unsend This Message'));

    }

    public function deleteMessage(Request $request)
    {
        $msg = Message::find($request['id']);
        $user = Auth::user();

        if ($msg) {
            $status = $user->msgStatus($msg);
            if ($status) {
                $status->status = 4;
                $status->save();
                return response()->json(array('success' => true, 'status' => $status));
            }
        }
        return response()->json(array('success' => false, 'title' => 'something wrong happened', 'msg' => 'You Can\'t Delete This Message'));

    }

    public function deleteChat(Request $request)
    {
        $thread = Thread::find($request['id']);
        $user = Auth::user();
        if ($thread && $user->isPartInThread($thread)) {
            if ($user->deleteAllThreadMessages($thread)) {
                return response()->json(array('success' => true, 'thread' => $thread));
            }
        }
        return response()->json(array('success' => false, 'title' => 'something wrong happened', 'msg' => 'You Can\'t Delete that Chat'));

    }

    public function markALlMsgsAsSeen()
    {
        $user = Auth::user();
        $user->markALlMsgsAsSeen();
        return response()->json(array('success' => true));

    }

    public function startGroupChat(Request $request)
    {
        $receipents = [];
        $user = Auth::user();
        foreach ($request['ids'] as $id) {
            $reciepent = User::find($id);
            if ($user->canSendMessage($id)) {
                $receipents[] = $reciepent;
            }
        }

        if (count($receipents) >= 2) {
            $thread = new Thread();
            $thread->is_group = 1;
            $thread->latest_used = Carbon::now()->toDateTimeString();
            $thread->save();
            $thread->initializeChatSettings();

            $user->participateInThread($thread, $user);
            $user->addToGroupChat($thread, $receipents);
            // foreach ($receipents as $receipent) {
            //   $obj = new stdClass;
            //   $obj->type = Message::ADDED;
            //   $obj->user = $receipent->only(['id','name','username']);
            //   $obj = json_encode($obj);
            //   $user->sendToThread($thread,$obj,[],2);
            //   $receipent->participateInThread($thread,$user);
            // }
            return response()->json(array('success' => true, 'thread_id' => $thread->id));

        } else {
            return response()->json(array('success' => false, 'title' => 'something wrong happened', 'msg' => 'To start group chat you must start with 2 people or more'));

        }
    }

    function getFullChatByThread($thread)
    {
        $myProfile = 1;
        $user = Auth::user();
        $section = 'messages';
        $title = 'Messages';
        if (request()->ajax()) {
            $html = view('user.chat', compact(['user', 'myProfile', 'section']))->render();
            return response()->json(['success' => true, 'html' => $html, 'title' => $title, 'thread' => $thread, 'chat' => 1]);
        }
        $chat = 1;
        return view('user.chat', compact(['user', 'myProfile', 'section', 'thread', 'chat']));
    }

    function getFullChatByUser($userId)
    {
        $myProfile = 1;
        $user = Auth::user();
        $section = 'messages';
        $title = 'Messages';
        if (request()->ajax()) {
            $html = view('user.chat', compact(['user', 'myProfile', 'section']))->render();
            return response()->json(['success' => true, 'html' => $html, 'title' => $title, 'chat' => 1, 'userId' => $userId]);
        }
        $chat = 1;
        return view('user.chat', compact(['user', 'myProfile', 'section', 'thread', 'chat', 'userId']));
    }
}
