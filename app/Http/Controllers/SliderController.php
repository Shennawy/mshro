<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function index()
    {
        $num = 1;
        $sliders = Slider::all();
        return view('admin.sliders.index',compact('sliders','num'));
    }
    public function create()
    {
        return view('admin.sliders.add');
    }

    public function store(Request $request,Slider $slider)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required',
        ],[
            'title.required' => 'عنوان السلايدر مطلوب',
            'image.required' => 'الصوره مطلوبه',
        ]);

        $newSlider = $slider->create([
            'title' => $request->title,
            'image' => 'noImage.png',
        ]);
        $user = \Auth::user();

        $name = $newSlider->id.'.'.request('image')->getClientOriginalExtension();
        request('image')->move(public_path('/uploads/sliders/'), $name);
        $newSlider->update(['image'=>$newSlider->id.'.'.request('image')->getClientOriginalExtension()]);
        return redirect('admincp/sliders/')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تمت اإضافة السلايدر بنجاح']));
    }

    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('admin.sliders.edit',compact('slider'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required',
        ],[
            'title.required' => 'عنوان السلايدر مطلوب',
            'image.required' => 'الصوره مطلوبه',
        ]);
        $slider = Slider::find($id);
        $data = [
            'title' => $request->title,
        ];

        $name = $slider->id.'.'.request('image')->getClientOriginalExtension();
        request('image')->move(public_path('/uploads/sliders/'), $name);
        $data['image'] = $slider->id.'.'.request('image')->getClientOriginalExtension();

        $slider->update($data);
        return redirect('admincp/sliders/')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تمت تعديل السلايدر بنجاح']));
    }

    public function destroy($id)
    {
        $slider = Slider::find($id)->delete();
        return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف السلايدر بنجاح']));
    }
}
