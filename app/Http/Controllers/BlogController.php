<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Http\Requests;

class BlogController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:blog-list', ['only' => ['index','show']]);
         $this->middleware('permission:blog-create', ['only' => ['create','store']]);
         $this->middleware('permission:blog-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:blog-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $num = 1;
        $blogs = Blog::all();
        return view('admin.blogs.index',compact('blogs','num'));
    }
    public function create()
    {
        return view('admin.blogs.add');
    }

    public function store(Request $request,Blog $blog)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
        ],[
            'title.required' => 'عنوان التدوينة مطلوب',
            'body.required' => 'المحتوى مطلوبه',
        ]);

        $newBlog = $blog->create([
            'user_id'=>Auth()->user()->id,
            'title' => $request->title,
            'body' => $request->body,
            'image' => 'noImage.png',
        ]);
        if($request->file('image')[0] != null){
            // $image = image_upload($blog,$request->file('image'),$newBlog->id,'/upload','blogs',false,false,360,360);
            $user = \Auth::user();

            $files = $user->saveFiles(request('image'));
            $newBlog->update(['image'=>$files[0]->url]);
        }else{
            $image = ['names' => [],'imgExt' => []];
        }
        if(!$newBlog->image){
            return redirect('admincp/blogs/')->withFlashMessage(json_encode(['success'=>false,'msg'=>'تمت اإضافة التدوينة بنجاح ولكن لم يتم إضافه غلاف المقال']));
        }
        return redirect('admincp/blogs/')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تمت اإضافة التدوينة بنجاح']));
    }

    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('admin.blogs.edit',compact('blog'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
        ],[
            'title.required' => 'عنوان التدوينة مطلوب',
            'body.required' => 'المحتوى مطلوبه',
        ]);
        $blog = Blog::find($id);
        $data = [
          "title" => $request->title,
          "body" => $request->body,
          'image' => 'noImage.png'
        ];

        if($request->file('image')[0] != null){
            $user = \Auth::user();

            $files = $user->saveFiles(request('image'));
            $data['image'] = $files[0]->url;
        }else{
            $image = ['names' => [],'imgExt' => []];
        }

        $blog->update($data);
        if(!$blog->image){
            return redirect('admincp/blogs/')->withFlashMessage(json_encode(['success'=>false,'تمت تعديل التدوينة بنجاح ولكن لم يتم إضافه صورة المقال']));
        }
        return redirect('admincp/blogs/')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تمت تعديل التدوينة بنجاح']));
    }

    public function destroy($id)
    {
        $blog = Blog::find($id)->delete();
        return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف التدوينة بنجاح']));
    }

    public function viewAll(){
        $blogs = Blog::all();
        return view('front.blogs.index',compact('blogs'));
    }
    public function viewSingle($id){
        $blog = Blog::findOrFail($id);
        return view('front.blogs.single',compact('blog'));
    }
}
