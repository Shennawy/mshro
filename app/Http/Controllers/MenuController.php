<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;

class MenuController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:menu-list', ['only' => ['index','show']]);
        $this->middleware('permission:menu-create', ['only' => ['create','store']]);
        $this->middleware('permission:menu-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:menu-delete', ['only' => ['destroy']]);
    }

    public function index(){
        $menus = Menu::all();
        return view('admin.menus.index',compact('menus'));
    }
    public function store(Request $request,Menu $menu){
        $this->validate($request,[
            'title'             => 'required|max:50',
            'order_by'          => 'required|integer',
            'link'              => $request->status != 2 ? 'required' : '' ,
        ],[
            'title.required'    => 'عنوان القائمة مطلوب',
            'title.max'         => 'يجب أن لا يزيد النص عن 50 حرف',
            'link.required'    => 'هذه الخانه مطلوبه',
            'order_by.required'    => 'خانة الترتيب مطلوبه',
            'order_by.integer'    => 'أكتب ارقام فقط',
        ]);
        $parent_id = $request->mainMenu != "" ? $request->parent_id :  0 ;
        $request->target == "" ? $target = 0 : $target = 1 ;
        $request->parent_id == "" ? $target = 0 : $target = 1 ;
        $data = [
            'parent_id' => $parent_id,
            'title' => $request->title,
            'link' => $request->link,
            'target' => $target,
            'status' => $request->status,
            'order_by' => $request->order_by,
            'type' => 1
        ];
        Menu::create($data);
        return redirect('admincp/menus')->withFlashMessage('تمت الإضافة بنجاح');
    }
    public function edit($id){
        $oneMenu = Menu::find($id);
        return view('admin.menus.edit',compact('oneMenu'));
    }
    public function update($id,Request $request){
        $menuUp = Menu::find($id);
        $this->validate($request,[
            'title'             => 'required|max:50',
            'order_by'          => 'required|integer',
            'link'              => $request->status != 2 ? 'required' : '' ,
        ],[
            'title.required'    => 'عنوان القائمة مطلوب',
            'title.max'         => 'يجب أن لا يزيد النص عن 50 حرف',
            'link.required'    => 'هذه الخانه مطلوبه',
            'order_by.required'    => 'خانة الترتيب مطلوبه',
            'order_by.integer'    => 'أكتب ارقام فقط',
        ]);
        $request->mainMenu == "" ? $parent_id = $request->parent_id : $parent_id = 0 ;
        $request->target == "" ? $target = 0 : $target = 1 ;
        $data = [
            'parent_id' => $parent_id,
            'title' => $request->title,
            'link' => $request->link,
            'target' => $target,
            'status' => $request->status,
            'order_by' => $request->order_by,
            'type' => 1
        ];
        $menuUp->update($data);

        // $menuUp->fill(['parent_id' => $parent_id,'target' => $target])->save();
        // $menuUp->fill(array_except($request->all(),['active','target']) )->save();
        return redirect('admincp/menus')->withFlashMessage('تم التعديل بنجاح');
    }
    public function destroy($id){
        Menu::find($id)->delete();
        foreach(Menu::where('parent_id',$id)->get() as $menu){
            foreach(Menu::where('parent_id',$menu->id)->get() as $subMenu){
                $subMenu->delete();
            }
            $menu->delete();
        }
        return 'done';
    }
    public function order(Request $r){
        $orderByNum = 1;
        foreach (json_decode($r->order) as $val) {
            if(isset($val->children)){
                $childOrderNum = 1;
                foreach ($val->children as $child) {
                    if(isset($child->children)){
                        $child2OrderNum = 1;
                        foreach ($child->children as $child2) {
                            Menu::find($child2->id)->update(['parent_id'=>$child->id,'order_by'=>$child2OrderNum]);
                            $child2OrderNum++;
                        }
                        Menu::find($child->id)->update(['parent_id'=>$val->id,'order_by'=>$childOrderNum]);
                    }else{
                        Menu::find($child->id)->update(['parent_id'=>$val->id,'order_by'=>$childOrderNum]);
                    }
                    $childOrderNum++;
                }
                Menu::find($val->id)->update(['parent_id'=>0,'order_by'=>$orderByNum]);
            }else{
                Menu::find($val->id)->update(['parent_id'=>0,'order_by'=>$orderByNum]);
            }
            $orderByNum++;
        }
        return back()->withFlashMessage('تم حفظ الإعدادات بنجاح');
    }

}
