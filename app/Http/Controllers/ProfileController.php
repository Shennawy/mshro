<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Work;
use Auth;
class ProfileController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index($profile)
  {
    $profile = \App\User::findOrFail($profile);
    $user = $profile;
    return view('user.profile',compact('user'));
  }
  function getWorks($profile){
    $profile = \App\User::findOrFail($profile);
    $user = $profile;
    $works = $user->Works;
    return view('user.projects',compact('works','user','test'));

  }
  function editProf(){
    $user = Auth::user();
    return view('user.edit-profile',compact('user'));
  }
  function saveProf($profile){
    $user = Auth::user();
    $prof = $user->Profile;
    $prof->fill(request()->except(['_token','name']));
    $prof->save();
    return redirect()->back();
  }
  function addWork($profile){
    return view('user.add-project');
  }
  function saveWork($profile){
    $user = Auth::user();
    $work = new Work();
    if(request('id')){
      $work = Work::where('user_id',Auth::user()->id)->findOrFail(request('id'));
    }
    $work->user_id = Auth::user()->id;
    $data = request()->except(['_token']);
    $work->fill($data);
    $work->save();
    return redirect("users/$user->id/works");

  }
  function editWork($profile,$id){
    $work = Work::where('user_id',Auth::user()->id)->findOrFail($id);
    return view('user.edit-project',compact('work'));
  }

  function removeWork($profile){
    $work = Work::where('user_id',Auth::user()->id)->findOrFail($id);
    if($work){
      $work->delete();
    }
    return redirect()->back();
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {

  }

}
