<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Category;
use App\Post;
use App\Offer;
class PostController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function save()
  {
      $post = new \App\Post();
      if(request('id')){
        $post = Post::findOrFail(request('id'));
        if($post->user_id != Auth::user()->id)
          $post = Post::findOrFail('undefined');

      }
      $data = request()->except(['_token']);
      $data['user_id'] = Auth::user()->id;
      $cat = \App\Category::find(1);
      if(!$cat){
        $cat =  new \App\Category();
        $cat->name = "";
        $cat->save();
      }
      $data['cat_id'] = 1;
      $post->fill($data);
      $post->save();
      return redirect('/posts/'.$post->id);
  }
  function add(){
    return view('user.add-post');
  }
  function saveOffer(){
    $offer = new \App\Offer();
    if(request('id')){
      $offer = Offer::findOrFail(request('id'));
      if($offer->user_id != Auth::user()->id)
        $offer = Offer::findOrFail('undefined');

    }
    $data = request()->except(['_token']);
    $data['user_id'] = Auth::user()->id;
    $offer->fill($data);
    $offer->save();
    $post = Post::find($data['post_id']);
    // dd($offer);
    return redirect('/posts/'.$post->id);
  }
  function getEdit($id){
    $post = Post::findOrFail($id);
    $user = Auth::user();

    if($post->user_id != $user->id){
      return redirect('/404');
    }
    return view('user.edit-post',compact('post'));
  }
  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $post = Post::findOrFail($id);
    return view('posts.single-post',compact('post'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {

  }

}
