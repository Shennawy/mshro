<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //
    const IMAGES = 1;
const NOTIF = 2;
const ADDED = 3;
const LEAVE = 0;
const CHANGED_COLOR = 4;
const FRIENDS = 5;
const FRIEND = 5;

  //protected $MULTI_IMG = 2;
  protected $fillable = ['message'];
  public function user()
  {
      return $this->belongsTo(User::class);
  }
  public function Thread()
{
  // Your relation type must be 'Belongs to', not 'Has one'
  return $this->belongsTo(Thread::class);  // or $this->belongsTo('App\Portfolio');
}
public function Files(){
return $this->morphMany(ModelFile::class,'model');
}



}
