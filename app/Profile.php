<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    protected $table = 'profiles';
    public $timestamps = true;
    protected $fillable = array('user_id', 'age', 'address', 'skills', 'head_line', 'bio', 'experience', 'image', 'data');

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    function getImageAttribute($image){
      return url($image) ?? url('/front/images/main/user.png');
    }
}
