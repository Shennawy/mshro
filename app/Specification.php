<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specification extends Model 
{

    protected $table = 'specifications';
    public $timestamps = true;
    protected $fillable = array('name', 'type', 'cat_id');

    public function Category()
    {
        return $this->belongsTo('App\Category', 'cat_id');
    }

    public function Options()
    {
        return $this->hasMany('App\SpecificationOption', 'specification_id');
    }

}