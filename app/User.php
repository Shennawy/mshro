<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\Chat;
use App\Traits\CanUploadFiles;
class User extends Authenticatable
{
    use Notifiable,
        SoftDeletes,Chat,CanUploadFiles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $table = 'users';
     public $timestamps = true;
     protected $fillable = ['name', 'email', 'password', 'status', 'type'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    function createProfile(){
      $profile = new \App\Profile();
      $profile->user_id = $this->id;
      $profile->save();
    }
    public function Profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function Posts()
    {
        return $this->hasMany('App\Post', 'user_id');
    }

    public function Offers()
    {
        return $this->hasMany('App\Offer', 'user_id');
    }

    public function Works()
    {
        return $this->hasMany('App\Work', 'user_id');
    }
}
