<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Activitylog\LogsActivity;

class SiteSetting extends Model 
{
    protected $table = 'settings';
    public $timestamps = false;
    protected $fillable = ['slug', 'name', 'value', 'type', 'module', 'orderBy'];

    public static function upSetImg($requestImg,$inputName = 'logo',$imgName = 'logo',$folerName ='logo',$out=null){
        $image = $requestImg;
        $destinationPath = public_path().'/upload/'.$folerName;
        if($out == true){
            $imageName = time().$imgName.'.png';
            $image->move($destinationPath,$imageName);
            return $imageName;
        }
        $imageName = $imgName.'.png';
        if(!in_array($imageName,['waterMarkTest.png'])){
            $image->move($destinationPath,$imageName);
            $settigsupdate = siteSetting::where('name',$inputName)->get()->first()->update(['value' => $imageName]);
        }
    }

}