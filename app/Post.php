<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{

    protected $table = 'posts';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('user_id', 'cat_id', 'title', 'desc', 'type', 'status', 'tags', 'price', 'visibility');

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function Offers()
    {
        return $this->hasMany(Offer::class);
    }

    public function Category()
    {
        return $this->belongsTo('App\Category', 'cat_id');
    }

}
