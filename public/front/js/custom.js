//wow
new WOW().init();


//user list
$(".user_head").click(function(){
    $(".user-div").toggleClass("active-user-div");
});


$(function() {
    var $wine = $(window); // or $box parent container
    var $boxe = $(".user-div");
    $wine.on("click.Bst", function(event) {
        if (
            $boxe.has(event.target).length === 0 && //checks if descendants of $box was clicked
            !$boxe.is(event.target) //checks if the $box itself was clicked
        ) {    $(".user-div").removeClass("active-user-div");

        }
    });
});


//responsive nav
$(".nav-icon").click(function() {
    $(".side-navbar").toggleClass("open-nav");
    $(this).toggleClass("active-bar");

});

//responsive nav
$(".menu-item-has-child").click(function() {
    $(this).toggleClass("active");
    $(this).find(".drop-menu-div").slideToggle();
    $(this).siblings(".menu-item-has-child").find(".drop-menu-div").slideUp();
    $(this).siblings(".menu-item-has-child").removeClass("active");

});



$(function() {
    var $win = $(window); // or $box parent container
    var $box = $(".nav-icon,.side-navbar");
    $win.on("click.Bst", function(event) {
        if (
            $box.has(event.target).length === 0 && //checks if descendants of $box was clicked
            !$box.is(event.target) //checks if the $box itself was clicked
        ) {
            $(".side-navbar").removeClass("open-nav");
            $(".nav-icon").removeClass("active-bar");
        }
    });
});

//header height
$(document).ready(function () {
    var headerHeight = $('header').outerHeight();
    $('body').css('padding-top', headerHeight);
});

//nav scroll
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    var headheight = 150;
    //>=, not <=
    if (scroll >= headheight) {
        //clearHeader, not clearheader - caps H
        $("header").addClass("header-fixed");


    } else {
        $("header").removeClass("header-fixed");
    }
});

//vedio



$(".vedio-grid").hover(function(){
    $(this).addClass("remove-vd-effect");
});

// text typing

function typeText(element, speed) {
    //   save text to write
    var textToWrite = element.textContent;

    //   Remove text from element
    element.innerHTML = "";

    //   size of text for cicle
    var textSize = textToWrite.length;

    //   Setting up the text on screen
    for (var i = 0; i < textSize; i++) {
        (function(i) {
            setTimeout(function() {
                element.innerHTML += textToWrite.charAt(i);
            }, speed * i);
        })(i);
    }
}

var elementToWrite = document.getElementById("typing-text");

var speedOfEffect = 100;

this.typeText(elementToWrite, speedOfEffect);




//pagination

$(function() {

    /* initiate the plugin */
    $("div.holder1").jPages({
        containerID: "itemcontainer",
        perPage: 4
    });

    $("div.holder2").jPages({
        containerID: "itemcontainer2",
        perPage: 10
    });


});


//img src change
$(".converted-img").each(function(i, elem) {
    var img_conv = $(elem);
       img_conv.parent(".full-width-img").css({
        backgroundImage: "url(" + img_conv.attr("src") + ")"
    });
  });

//file-upload

$('#chooseFile').bind('change', function() {
    var filename = $("#chooseFile").val();
    if (/^\s*$/.test(filename)) {
        $(".file-upload2").removeClass('active');
        $("#noFile").text("");
    } else {
        $(".file-upload2").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
    }
});

//contact filter
$(document).ready(function(){
  $("#filter-input").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".chat-users li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });


});


//attch open
$("#attach-icon").click(function() {
    $(this).toggleClass("active")
    $(this).parent().find(".attach-list").slideToggle();
});

$(function() {
    var $winef = $(window); // or $box parent container
    var $boxef = $(".up-div");
    $winef.on("click.Bst", function(event) {
        if (
            $boxef.has(event.target).length === 0 && //checks if descendants of $box was clicked
            !$boxef.is(event.target) //checks if the $box itself was clicked
        ) {

            $("#attach-icon").removeClass("active");
            $(".attach-list").fadeOut("slow");
        }
    });
});


//file upload
$(document).ready(function() {
    $('input[type="file"]').imageuploadify();
})


//replay-div
$(".add-replay-btn").click(function(){
    $(this).parent().parent().parent().find(".replay-form").slideDown();
});


//owl-carousel

$(".first-owl.owl-carousel").owlCarousel({
    loop: true,
    margin: 0,
    rtl: true,
    items: 1,
    dots:true,
    nav:false,
    autoplay: true,
    mouseDrag: true,
    touchDrag: true,
    animateOut: "slideOutDown",
    animateIn: "fadeInDown"

});


//counter
$('.stat-number').each(function () {
    var size = $(this).text().split(".")[1] ? $(this).text().split(".")[1].length : 0;
    $(this).prop('Counter', 0).animate({
       Counter: $(this).text()
    }, {
       duration: 7000,
       step: function (func) {
          $(this).text(parseFloat(func).toFixed(size));
       }
    });
 });

//form validtion
(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();



function sendMsgKeyUp(e){
  if(event.key == 13){
    sendMsg(e);
  }
}
function msgComponent(msg){
  return `<!--start chat-conver-->
  <div class="chat-conver sender" pending>
      <div class="chat-img full-width-img">
          <img src="`+$('#profile-pic').val()+`" class="converted-img" alt="person" />
      </div>
      <div class="chat-bubble">
          `+msg+`
      </div>
  </div>
`
}
function sendMsg(e){
  msg = $('#msg-body').val();
  files = $('#msg-file').prop('files');
  user = parseInt($('#user').val());
  if(user){
  formData = new FormData;

    formData.append('recipant', user)
    formData.append('body', msg)
    if(files)
    formData.append('files', files)
    msg = msgComponent(msg)
    $('.inner-chat-div').append(msg);
    $('#msg-body').val('')
    $('.inner-chat-div').scrollTop(5000*100);
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: '/messages/send', // point to server-side PHP script
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        type: 'POST',
        success: function(data) {
          data = JSON.parse(data);
          if (data.success) {
            $('[pending]').first().replaceWith(data.html)
            console.log(data)
          }
        }
      })
  } else {
    alert('اختر شخص لارسال الرسالة')
  }

}
