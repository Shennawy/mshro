function toastrNotifyResponse(data) {
    var defMsg = 'حدث خطأ ما فضلا حدث الصفحة و حاول مره أخرى';
    toastr.options.timeOut = '2500';
    toastr.options.positionClass = 'toast-bottom--center';
    Command: toastr[data.success ? "success" : "error"](data.msg ? data.msg : defMsg);
}

$(document).ready(function () {
    // called falsh message session
    if ($('#d-fmsg').text() != '') {
        toastrNotifyResponse(JSON.parse($('#d-fmsg').text()));
    }
});

