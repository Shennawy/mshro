    var UINestable = function () {

        var updateOutput = function (e) {
            var list = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };

        return {
            //main function to initiate the module
            init: function () {

                // activate Nestable for list 1
                $('#nestable_list_1').nestable({group: 1,maxDepth: 3,collapsedClass  : 'dd-collapsed'}).on('change', updateOutput).nestable('collapseAll');
                // output initial serialised data
                updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));
                // expandAll&collapseAll
                $('#nestable_list_menu').on('click', function (e) {
                    var target = $(e.target),
                        action = target.data('action');
                    if (action === 'expand-all') {
                        $('.dd').nestable('expandAll');
                    }
                    if (action === 'collapse-all') {
                        $('.dd').nestable('collapseAll');
                    }
                });

                // activate Nestable for list 3
                $('#nestable_list_3').nestable({group: 1,maxDepth: 1,collapsedClass  : 'dd-collapsed'}).on('change', updateOutput).nestable('collapseAll');
                // output initial serialised data
                updateOutput($('#nestable_list_3').data('output', $('#nestable_list_3_output')));
                // expandAll&collapseAll
                $('#nestable_list_menu').on('click', function (e) {
                    var target = $(e.target),
                        action = target.data('action');
                    if (action === 'expand-all') {
                        $('.dd').nestable('expandAll');
                    }
                    if (action === 'collapse-all') {
                        $('.dd').nestable('collapseAll');
                    }
                });
            }
        };
    }();
    jQuery(document).ready(function() {    
       UINestable.init();
    });
    
    $(document).on('change','select[name=mainCat]',function(){
        $(this).closest('form').find('input[name="title[ar]"]').val($(this).find('option:selected').data('ar'));
        $(this).closest('form').find('input[name="title[en]"]').val($(this).find('option:selected').data('en'));
        $(this).closest('form').find('input[name=link]').val($(this).find('option:selected').data('ar'));
    });

    $('.dd-handle').each(function(){
        if($(this).next().hasClass('dd-list')){
            $(this).css('background','#eaeaea');
        }
        if($(this).data('parent') == 0){
            $(this).css('background','#ccc');
        }
    });
    // $('input[name=mainMenu]').removeAttr('checked');
    $(document).on('click','.uppercase',function(){
        if($(this).hasClass('red')){
            $(this).data('action','expand-all');
            $(this).removeClass('red').addClass('green');
            $(this).html(' فتح الكل <i class="fa fa-angle-down"></i>');
        }else{
            $(this).data('action','collapse-all');
            $(this).removeClass('green').addClass('red');
            $(this).html(' إغلاق الكل <i class="fa fa-angle-up"></i>');
        }
    });

    
    if($('select[name=status]').val() == 2){
        $('input[name=link]').hide();
        $('input[name=target]').attr('readonly',false);
        $('input[name=mainMenu]').attr('checked',true);
            $('.notPlatformLink').hide();
    }
    if($('select[name=status]').val() == 4){
        $('input[name=link]').closest('.form-group').find('label').text('أكتب معرف الصفحة');
        $('input[name=link]').attr('placeholder','مثال ( gift )');
        $('input[name=link]').attr('readonly',false);
        $('.platformLink').hide();
        $('.notPlatformLink').show();
    }

    $(document).on('change','select[name=status]',function(){
        $('input[name=link]').val('');
        $('input[name=target]').attr('readonly',false);
        $('input[name=mainMenu]').attr('checked',false);
        $('input[name=link]').show();
        if($(this).val() == 1){
            $('input[name=link]').closest('.form-group').find('label').text('الرابط');
            $('input[name=link]').attr('placeholder','مثال ( google.com ) بدون https://');
            $('.platformLink').hide();
            $('.notPlatformLink').show();
            $('select[name=pageModel]').hide()
        }else if($(this).val() == 2){
            $('input[name=link]').attr('readonly',true);
            $('input[name=mainMenu]').attr('checked',true);
            $('input[name=target]').attr('readonly',true);
            $('select[name=parent_id]').attr('disabled',true);
            $('.platformLink').hide();
            $('.notPlatformLink').hide();
            $('select[name=pageModel]').hide()
            // $('.notPlatformLink').show();
        }else if($(this).val() == 3){
            $('.platformLink').show();
            $('.notPlatformLink').hide();
            $('select[name=pageModel]').hide()
        }else if($(this).val() == 4){
            $('input[name=link]').closest('.form-group').find('label').text('أكتب معرف الصفحة');
            $('input[name=link]').attr('placeholder','مثال ( gift )');
            $('input[name=link]').attr('readonly',false);
            $('.platformLink').hide();
            $('.notPlatformLink').show();
            $('select[name=pageModel]').show()
        }
    });
    $(document).on('change','select[name=pageModel]',function(){
        if($(this).val() == "#"){
            $('input[name=link]').val('');
            $('input[name="title[ar]"]').val('');
            $('input[name="title[en]"]').val('');
            return false;
        }
        var url = $('#d-root').data('root')+'/admincp/pagesMenu/'+$(this).val();
        $.get(url,function(data){
            $('input[name=link]').val(data.pLink);
            $('input[name="title"]').val(data.pName);
        });
    });
    if($('input[name=mainMenu]').is(':checked')){
        $('select[name=parent_id]').attr('disabled',true);
    }

    $(document).on('change','input[name=mainMenu]',function(){
        if($('input[name=mainMenu]').is(':checked')){
            $('select[name=parent_id]').attr('disabled',true);
        }else{
            $('select[name=parent_id]').attr('disabled',false);
        }
    });
    // delete menu 
    $(document).on("click",".delMenu",function(e) {
        e.preventDefault();
        var a=$(this),token = $(this).data('token'),
        id = $(this).attr('bankId'),
        route = $(this).attr('href');
        $.ajax({
            url:route,
            type: 'post',
            data: {_method: 'delete', _token :token},
            success:function(data){
                a.closest('li').remove();
                if (data=="done"){
                    toastr.options.timeOut = '6000';
                    toastr.options.positionClass = 'toast-bottom-left';
                    Command: toastr["success"]("تم الحذف بنجاح")
                }
            } 
        });
        return;
    });
    // validation
    $(document).on("click",".menuForm",function(e) {
        var done = true;
        $(this).closest('form').find('input[type=text]').each(function(){
            if(!$(this).is(":hidden")){
                if($(this).val() == ""){
                    alert('فضلا أكمل البيانات - ' + $(this).attr('placeholder') );
                    $(this).focus();
                    if($(this).attr('id') == 'lat' || $(this).attr('id') == 'lng'){
                        $('#addCity').modal('toggle');
                    }
                    e.preventDefault();
                    return done = false;
                }
            }
            $(this).closest('form').find('select').each(function(){
                if(!$(this).is(":hidden")){
                    if($(this).val() == "#" || $(this).val() == null){
                        alert('فضلا أكمل البيانات - ' + $(this).find("option:first-child").text());
                        $(this).focus();
                        e.preventDefault();
                        return done = false;
                    }
                }
                return done;            
            });
            return done;
        });
        return done;
    });