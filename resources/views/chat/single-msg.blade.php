<!--start chat-conver-->
<div class="chat-conver {{Auth::user()&&$msg->User->id == Auth::user()->id ? 'sender' : 'receiver'}}">
    <div class="chat-img full-width-img">
        <img src="{{$msg->User->Profile->image}}" class="converted-img" alt="person" />
    </div>
    <div class="chat-bubble">
        {{$msg->body}}
    </div>
    <span class="chat-time">{{$msg->created_at}}</span>
</div>
