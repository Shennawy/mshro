@extends('front.layouts.master')

@section('content')
  <!--start pages-head
          ================-->
    <section class="pages-head">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="white-title sec-head center-sec wow fadeIn"><span>الرسائل</span></h1>
                    <p class="white-prg wow fadeIn">هنا تجد رسائلك و يمكنك التواصل بين اعضاء الموقع</p>
                </div>
            </div>
        </div>
    </section>
    <!--end pages-head-->
    @php
      if(Auth::user()){
        Auth::user()->unreadNotifications()
                            ->orWhere('type','LIKE','%'.'MessageReceived'.'%')->get()->markAsRead();
      }
    @endphp

        <!--start login-pg
              ================-->

        <section class="login-pg text-right marg-sec">
            <div class="container">
                <div class="row">
                    <!--start chat-contant-grid-->
                    <div class="col-lg-3 col-md-6 chat-contant-grid wow fadeIn">
                        <!--start chat-content-list-->
                        <div class="chat-content-list wow fadeIn">
                            <h2 class="chat-head wow fadeInDown text-center"><span>قائمة الإتصال</span></h2>
                            <!--start search-form-->
                            <form class="needs-validation" novalidate>
                                <div class="chat-search">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="filter-input" placeholder="البحث عن متصلون." required />
                                        <div class="invalid-feedback">
                                            من فضلك أدخل كلامات بحث صحيحة
                                        </div>
                                        <button type="submit" class="form-btn search-btn2"><i class="fa fa-search"></i></button>

                                    </div>
                                </div>
                            </form>
                            <!--end search-form-->

                            <ul class="list-unstyled chat-users">
                              @foreach (Auth::user()->getChatPeople() as $user)
                                <li chat-name="{{$user->name}}">
                                    <a href="{{url("messages/user/".$user->id)}}">
                                        <div class="chat-img full-width-img">
                                            <img src="{{$user->Profile->image}}" class="converted-img" alt="person" />
                                        </div>
                                        <div class="side-contact-chat">
                                            <h3>{{$user->name}}</h3>
                                            <p>{{$user->Profile->head_line}}</p>
                                        </div>
                                    </a>
                                </li>
                              @endforeach



                            </ul>
                        </div>
                        <!--end chat-content-list-->
                    </div>
                    <!--end chat-contant-grid-->

                    <!--start main-chat-grid-->
                    <div class="col-lg-6 col-md-12 main-chat-grid wow fadeIn">
                        <div class="main-chat-div">
                          @if(isset($id)&&$id)
                          <input type="hidden" id="user" target="user" value="{{$id}}">
                          @php
                            $user = \App\User::find($id);
                            $messages = Auth::user()->getMessagesWithUser($user);
                          @endphp
                        @endif

                            <div class="inner-chat-div" current-thread="0">
                              @if(isset($messages))
                                @foreach ($messages as $msg)
                                  @include('chat.single-msg')
                                @endforeach
                              @endif


                            </div>
                            <div class="ch-attch">
                                <div class="ch-att">
                                    <textarea class="form-control" id="msg-body" onkeyup="sendMsgKeyUp(this)" name="message" data-emojiable="true"></textarea>
                                    <div class="up-div">
                                        <span id="attach-icon" class="chat-shapes"><i class="fa fa-link"></i></span>
                                        <div class="attach-list">
                                            <div class="inner-attach">
                                                <input type="file" id="img-up1">
                                                <label for="img-up1" id="msg-file" class="chat-label"><i class="fa fa-folder-open"></i>ملف</label>
                                            </div>
                                            {{-- <div class="inner-attach">
                                                <input type="file" id="img-up2">
                                                <label for="img-up2" class="chat-label"><i class="fa fa-picture-o"></i>صورة</label>
                                            </div>
                                            <div class="inner-attach">
                                                <input type="file" id="img-up3">
                                                <label for="img-up3" class="chat-label"><i class="fa fa-video-camera"></i>فيديو</label>
                                            </div> --}}
                                        </div>
                                    </div>

                                    <button type="submit" onclick="sendMsg(this)" class="form-btn search-btn2"><i class="fa fa-paper-plane"></i></button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!--end login-form-grid-->


                    <!--start chat-contant-grid-->
                    <div class="col-lg-3 col-md-6 chat-contant-grid wow fadeIn">
                        <!--start chat-content-list-->
                        <div class="chat-content-list wow fadeIn">
                            <!--start chat-man-->
                            @if(isset($user) && $user)
                            <div class="chat-man wow fadeIn">
                                <a href="{{$user->Profile->image}}" class="html5lightbox">
                                    <div class="chat-img full-width-img">
                                        <img src="{{$user->Profile->image}}" class="converted-img" alt="person" />
                                    </div>
                                </a>
                                <h3 class="chat-man-name">{{$user->name}}</h3>
                                {{-- <span class="chat-man-status">متصل</span> --}}
                                <div class="login-list  user-chat-list wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                                    <ul class="list-unstyled">
                                        {{-- <li><a href="conditions.html"><i class="fa fa-search"></i> البحث عن محادثات </a></li>
                                        <li><a href="instruction.html"><i class="fa fa-edit"></i>تعديل الاسم المستعار</a></li>
                                        <li><a href="about.html"><i class="fa fa-paint-brush"></i>تغير الالوان</a></li> --}}
                                    </ul>
                                </div>
                            </div>
                          @endif
                            <!--end chat-man-->
                        </div>
                        <!--end chat-content-list-->
                    </div>
                    <!--end chat-contant-grid-->

                </div>
            </div>
        </section>

        <!--end login-pg-->

        <!--end login-pg-->
        @include('front.components.join-us')

@endsection
