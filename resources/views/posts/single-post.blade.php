@extends('front.layouts.master')

@section('content')
  <!--start pages-head
          ================-->
    <section class="pages-head">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="white-title sec-head center-sec wow fadeIn"><span>{{$post->title}}</span></h1>
                    {{-- <p class="white-prg wow fadeIn">{{$user->Profile->head_line}}</p> --}}
                    {{-- <a class="blue_btn custom_btn wow fadeInUp" href="#">متابعة العضو</a> --}}
                </div>
            </div>
        </div>
    </section>
    <!--end pages-head-->
  <!--start login-pg
        ================-->

  <section class="login-pg text-right marg-sec">
      <div class="container">
          <div class="row">

              <!--start main-chat-grid-->
              <div class="col-lg-9 col-md-12 main-view-grid">
                  <div class="main-view-div">
                      <div class="inner-view-div">

                          {{-- <div class="tag-div wow fadeIn">

                              <span>{{$post->statusDesc}}</span>
                          </div> --}}

                          <h3 class="dark_title wow fadeIn">{{$post->title}}</h3>
                          <div class="tag-lists wow fadeIn">
                              <ul class="list-inline">
                                  {{-- <li><a href="#"><i class="fa fa-folder"></i>{{$post->cat_id}}</a></li> --}}
                                  {{-- <li><i class="fa fa-clock-o"></i>بدأ تنفيذه منذ ساعة و 22 دقيقة</li> --}}
                              </ul>
                          </div>
                          <!--start line-shape-sec-->
                          <!--put active for progress steps-->
                          <div class="line-shape-sec wow fadeIn">
                              <div class="line-shape">
                                  <div class="line-content {{$post->status==1?'active':''}}">
                                    @if($post->status==1)
                                      <span class="progress-bar-item"></span>
                                    @endif
                                      <span class="progress-title"> مرحلة تلقي العروض</span>
                                  </div>

                                  <div class="line-content {{$post->status==2?'active':''}}">
                                    @if($post->status==2)
                                      <span class="progress-bar-item"></span>
                                      <span class="progress-bar-item"></span>
                                    @endif
                                      <span class="progress-title"> مرحلة التنفيذ</span>
                                  </div>
                                  <div class="line-content {{$post->status==3?'active':''}}">
                                    @if($post->status==3)
                                      <span class="progress-bar-item"></span>
                                    @endif
                                      <span class="progress-title"> مرحلة التسليم</span>
                                  </div>
                              </div>
                          </div>
                          <!--end line-shape-sec-->

                          <!--start view-project-details-->
                          <div class="view-project-details">
                              <h2 class="border-title wow fadeIn">تفاصيل المشروع</h2>
                              <p class="dark-prg wow fadeIn">{{$post->desc}}</p>

                              <div class="login-form-div add-project-form project-view-form">
                                  <h2 class="border-title wow fadeIn">أضف عرضك الآن</h2>
                                  <form class="needs-validation row" novalidate action="{{url('posts/offer')}}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="post_id" value="{{$post->id}}">
                                      <div class="form-group  col-lg-4 col-md-6 wow fadeIn">
                                          <label>مدة التسليم (بالأيام)<span>*</span></label>
                                          <input type="text" name="duration" class="form-control" >


                                          <div class="invalid-feedback">
                                              من فضلك أدخل مدة التسليم
                                          </div>
                                      </div>

                                      <div class="form-group  col-lg-4 col-md-6 wow fadeIn">
                                          <label>قيمة العرض (بالريال السعودي)<span>*</span></label>
                                          <input type="text" name="price" class="form-control" >

                                          <div class="invalid-feedback">
                                              من فضلك أدخل قيمة العرض
                                          </div>
                                      </div>



                                      <div class="form-group  col-lg-4 col-md-6 wow fadeIn">
                                          <label> مستحقاتك <span>*</span></label>
                                          <input type="text" class="form-control" disabled>
                                          <span class="project-input-note">بعد خصم عمولة <b>موقع مشروع نت</b></span>

                                      </div>



                                      <div class="form-group  col-12 wow fadeIn">
                                          <label>تفاصيل العرض<span>*</span></label>
                                          <textarea class="form-control" name="body" required></textarea>
                                          <div class="invalid-feedback">
                                              من فضلك أدخل تفاصيل المشروع
                                          </div>
                                      </div>

                                      <div class="form-group col-sm-12 col-md-6 col-lg-7 wow fadeIn">
                                          <div class="file-upload2">
                                              <div class="file-select">
                                                  <div class="file-select-button form-icon" id="fileName"><i class="fa fa-cloud-upload"></i></div>
                                                  <div class="file-select-name" id="noFile">أرفق ملفات</div>
                                                  <input type="file" name="files" name="chooseFile" class="form-control" id="chooseFile" required>
                                                  <div class="invalid-feedback">
                                                      من فضلك أرفق ملفات

                                                  </div>
                                              </div>
                                          </div>

                                      </div>

                                      <div class="form-group  col-12 wow fadeIn">
                                          <ul class="list-unstyled circlelists">
                                              <li> لا تستخدم وسائل تواصل خارجية</li>
                                              <li> لا تضع روابط خارجية، قم بالاهتمام بمعرض أعمالك بدلا منها</li>
                                              <li> اقرا هنا كيف تضيف عرضا مميزا على أي مشروع</li>
                                          </ul>
                                      </div>






                                      <div class="form-group submit-form-group  col-12 wow fadeIn">
                                          <button type="submit" class="blue_btn custom_btn">أضف عرضك</button>
                                      </div>




                                  </form>
                              </div>

                          </div>
                          <!--end view-project-details-->

                          <!--start comments-div-grid-->
                          <div class="comments-div-grid">
                              <h2 class="border-title wow fadeIn">العروض المقدمة</h2>
                              <div id="itemcontainer" class="proj-pagination">
                              
                                @foreach ($post->Offers as $key => $offer)
                                  @include('posts.single-offer')
                                @endforeach

                              </div>
                              <div class="holder_pager holder1"></div>



                          </div>
                          <!--end view-project-details-->




                      </div>
                  </div>

              </div>
              <!--end login-form-grid-->


              <!--start chat-contant-grid-->
              <div class="col-lg-3   project-side-div-grid chat-contant-grid wow fadeIn">
                  <!--start chat-content-list-->
                  <div class="project-side-div row wow fadeIn">

                      <!--start chat-man-->
                      <div class="chat-man col-lg-12 col-md-6 wow fadeIn">
                          <h3 class="blue-head">صاحب المشروع </h3>
                          <a href="{{$post->User->Profile->image}}" class="html5lightbox">
                              <div class="chat-img full-width-img">
                                  <img src="{{$post->User->Profile->image}}" class="converted-img" alt="person" />
                              </div>
                          </a>
                          <h3 class="chat-man-name">{{$post->User->name}}</h3>
                          {{-- <span class="chat-man-status"><i class="fa fa-map-marker"></i> مصر</span> --}}
                      </div>
                      <!--end chat-man-->

                      <!--start chat-man-->
                      <div class="chat-man col-lg-12 col-md-12 view-submit project-card-grid wow fadeIn">
                          <h3 class="blue-head">بطاقة المشروع </h3>
                          <div class="money-list wow fadeIn">
                              <ul class="list-unstyled">
                                  <li><span>الميزانية</span>{{$post->price}}</li>
                                  <li><span>مدة التنفيذ</span>{{$post->duration}}</li>
                                  <li><span>عدد العروض</span>{{$post->Offers->count()}}</li>
                              </ul>
                          </div>
                      </div>
                      <!--end chat-man-->
                      @if($post->status == 2)
                      <!--start chat-man-->
                      <div class="chat-man col-lg-12 col-md-6 view-submit project-card-submiter wow fadeIn">
                          <h3 class="blue-head">منفذ المشروع </h3>
                          <a href="images/main/chat.png" class="html5lightbox">
                              <div class="chat-img full-width-img">
                                  <img src="images/main/chat.png" class="converted-img" alt="person" />
                              </div>
                          </a>
                          <h3 class="chat-man-name">اسم المتصل</h3>
                          <div class="login-list user-chat-list wow fadeIn">
                              <ul class="list-unstyled">
                                  <li><i class="fa fa-folder"></i>تصميم وأعمال فنية وإبداعية</li>
                                  <li><i class="fa fa-map-marker"></i>مصر</li>
                              </ul>
                          </div>
                      </div>
                      <!--end chat-man-->
                    @endif

                  </div>
                  <!--end chat-content-list-->
              </div>
              <!--end chat-contant-grid-->

          </div>
      </div>
  </section>

  <!--end login-pg-->


@endsection
