<!--start comments-div-->
<div class="comments-div row   wow fadeIn">
    <div class="chat-man col-xl-2 col-md-4 text-center">
        <a href="{{$offer->User->Profile->image}}" class="html5lightbox">
            <div class="chat-img full-width-img" style="background-image:url({{$offer->User->Profile->image}})">
                <img src="{{$offer->User->Profile->image}}" class="converted-img" alt="person" />
            </div>
        </a>
        <h3 class="chat-man-name">{{$offer->User->name}}</h3>
        {{-- <ul class="comment-star-list list-inline">
            <li><i class="fa fa-star"></i></li>
            <li><i class="fa fa-star"></i></li>
            <li><i class="fa fa-star"></i></li>
            <li><i class="fa fa-star"></i></li>
            <li><i class="fa fa-star"></i></li>
        </ul> --}}
    </div>

    <div class="side-comments col-xl-9 col-md-8">
        <div class="tag-lists">
            <ul class="list-inline">
                {{-- <li><i class="fa fa-clock-o"></i>منذ ساعة و 22 دقيقة</li> --}}
                <li><a href="{{url('/user/'.$offer->User->id)}}"><i class="fa fa-folder"></i>{{$offer->User->Profile->head_line}}</a></li>
            </ul>
        </div>
        <p class="dark-prg">
            {{$offer->body}}

    </div>

</div>
<!--end comments-div-->
