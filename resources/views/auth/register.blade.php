@extends('layouts.app')

@section('content')
  @extends('front.layouts.master')

  @section('content')
    <!--start pages-head
            ================-->
      <section class="pages-head">
          <div class="container">
              <div class="row">
                  <div class="col-12">
                      <h1 class="white-title sec-head center-sec wow fadeIn"><span>دخول الاعضاء</span></h1>
                      <p class="white-prg wow fadeIn">نبذه مختصره عندخول الاعضاء فى موقع مشروع نت ..نبذه مختصره عندخول الاعضاء فى موقع مشروع نت </p>
                  </div>
              </div>
          </div>
      </section>
      <!--end pages-head-->

          <!--start login-pg
                ================-->

          <section class="services login-pg text-right marg-sec">
              <div class="container">
                  <div class="row">
                      <!--start service-grid-list-->
                      <div class="col-lg-4 col-md-12 service-grid-list">
                          <h2 class="sec-head wow fadeInDown"><span>مساعده</span></h2>
                          <!--start service-div-list-->

                          <div class="login-list wow fadeIn">
                              <ul class="list-unstyled">
                                  <li><a href="{{ route('register') }}"><i class="fa fa-warning"></i> لا املك حساب</a></li>
                                  <li><a href="{{ route('password.request') }}"><i class="fa fa-unlock"></i>فقدت كلمة المرور</a></li>
                                  <li><a href="#"><i class="fa fa-envelope"></i>لم يصلني رمز التفعيل</a></li>
                              </ul>
                          </div>
                          <!--end service-div-list-->

                          <!--start advert-img-->
                          <a href="#" class="advert-link wow fadeIn">
                              <div class="advert-img adv-div full-width-img wow fadeIn">
                                  <img src="/front/images/main/advertisment.png" alt="advertisment.png" class="converted-img" />
                              </div>
                          </a>
                          <!--start advert-img-->
                      </div>
                      <!--end service-grid-list-->

                      <!--start login-form-grid-->
                      <div class="col-lg-8 col-md-12 login-form-grid">
                          <div class="login-form-div">
                              <form class="needs-validation icons-form row" method="POST" action="{{ route('register') }}" novalidate>
                                {{ csrf_field() }}
                                  <div class="form-group social-login wow fadeIn col-12">
                                      <a href="" class="fc-icon"><i class="fa fa-facebook"></i><span>باستخدام الفيس بوك</span></a>
                                      <a href="" class="goo-icon"><i class="fa fa-google"></i><span>باستخدام جوجل</span></a>

                                  </div>
                                  <div class="form-group  col-12 wow fadeIn">
                                      <i class="fa fa-envelope login-form-icon"></i>
                                      <input type="text" name="name" required class="form-control" id="email_input1" placeholder=" اسمك">
                                      <div class="invalid-feedback">
                                    ادخل الاسم صحيح
                                </div>
                                      @if ($errors->has('name'))
                                          {{-- <span class="invalid-feedback" role="alert">
                                              <strong>{{ $errors->first('email') }}</strong>
                                          </span> --}}

                                      @endif

                                  </div>
                                  <div class="form-group  col-12 wow fadeIn">
                                      <i class="fa fa-envelope login-form-icon"></i>
                                      <input type="email" name="email" class="form-control" id="email_input1" aria-describedby="emailHelp" placeholder=" بريدك الالكتروني" required>
                                      <div class="invalid-feedback">
                                    من فضلك أدخل بريد الكتروني صحيح
                                </div>
                                      @if ($errors->has('email'))
                                          {{-- <span class="invalid-feedback" role="alert">
                                              <strong>{{ $errors->first('email') }}</strong>
                                          </span> --}}

                                      @endif

                                  </div>



                                  <div class="form-group  col-12 wow fadeIn">
                                      <i class="fa fa-lock login-form-icon"></i>
                                      <input type="password" name="password" class="form-control" id="password_input1" placeholder="كلمة المرور" required>
                                      <div class="invalid-feedback">
                                          من فضلك أدخل كلمة مرور صحيحة
                                      </div>
                                      @if ($errors->has('password'))
                                          {{-- <span class="invalid-feedback" role="alert">
                                              <strong>{{ $errors->first('password') }}</strong>
                                          </span> --}}
                                      @endif
                                  </div>
                                  <div class="form-group  col-12 wow fadeIn">
                                      <i class="fa fa-lock login-form-icon"></i>
                                      <input type="password" name="password_confirmation" class="form-control" id="password_input1" placeholder="تأكيد كلمة المرور" required>
                                      <div class="invalid-feedback">
                                          من فضلك أدخل كلمة مرور صحيحة
                                      </div>
                                      @if ($errors->has('password'))
                                          {{-- <span class="invalid-feedback" role="alert">
                                              <strong>{{ $errors->first('password') }}</strong>
                                          </span> --}}
                                      @endif
                                  </div>

                                  <div class="form-group col-12 condition-form-group wow fadeIn">
                                      <input type="checkbox" name="remember" class="accept-cond">
                                      <label class="condition-label">تذكرني</label>
                                  </div>


                                  <div class="form-group submit-form-group col-12 wow fadeIn">
                                      <button type="submit" class="blue_btn custom_btn">دخول</button>
                                  </div>




                              </form>
                          </div>
                      </div>
                      <!--end login-form-grid-->

                  </div>
              </div>
          </section>

          <!--end login-pg-->
          @include('front.components.join-us')

  @endsection

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
