@extends('admin.layouts.app')
@section('title','التحكم بالصفحات')
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('admincp')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="#" class="m-menu__link">
            <span class="m-menu__link-text">نظام الصفحات</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/admincp/pages/create')}}" class="m-menu__link">
            <span class="m-menu__link-text">إضافة صفحة جديده</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('content')
    <div class="m-portlet">
        <div class="m-portlet__body">
            <ul class="nav nav-tabs  m-tabs-line m-tabs-line--2x m-tabs-line--success" role="tablist">
                @foreach(parentPages() as $key => $tab)
                    <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link {{$key == 1 ? 'active' : '' }}" href="#tab_1_{{$key}}"
                           data-toggle="tab" role="tab">
                            {{$tab}}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach(parentPages() as $key => $tab)
                    <div class="tab-pane {{$key == 1 ? 'active' : '' }} fontawesome-demo" id="tab_1_{{$key}}"
                         role="tabpanel">
                        <div class="portlet-body">
                            <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                <thead>
                                <tr>
                                    <th>م</th>
                                    <th>إسم الصفحة</th>
                                    <th>المكان</th>
                                    <th>الحالة</th>
                                    <th>الأدوات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pages as $page)
                                    @if($page->type == $key)
                                        <tr>
                                            <td>{{$num++}}</td>
                                            <td>{{$page->pName}}</td>
                                            <td>{{parentPages()[$page->type]}}</td>
                                            <td class="text-center">
                                                @if($page->active == 1)
                                                    {{Form::open(array('method'=>'POST','url'=>'admincp/pages/'.$page->id.'/0'))}}
                                                    {{ csrf_field() }}
                                                    <input type="checkbox" name="my-checkbox"
                                                           onchange="this.form.submit()" checked>
                                                    {{Form::close()}}
                                                @else
                                                    {{Form::open(array('method'=>'POST','url'=>'admincp/pages/'.$page->id.'/1'))}}
                                                    {{ csrf_field() }}
                                                    <input type="checkbox" name="my-checkbox"
                                                           onchange="this.form.submit()">
                                                    {{Form::close()}}
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{url('/admincp/pages/'.$page->id.'/edit')}}"
                                                   class="btn btn-success"><i class="fa fa-edit"></i> التحكم </a>
                                                @if($page->type != 1)
                                                    <a id="delete{{$page->id}}" class="btn btn-danger" href="#"><i
                                                                class="fa fa-trash-o"></i> حذف </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
<!-- footer -->
@section('footer')
    <script type="text/javascript">
        @if(isset($page))
        @foreach($pages as $page)
        $('#delete{{$page->id}}').click(function () {
            swal({
                    title: "هل انت متأكد؟",
                    text: "انت لن تسطيع إسترجاع هذه البيانات مره أخرى !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'نعم , أحذفها !',
                    cancelButtonText: 'إلغاء !',
                    closeOnConfirm: false
                },
                function () {
                    document.location.href = "{{url('/admincp/pages/'.$page->id.'/delete')}}";
                });
        });
        @endforeach
        @endif
    </script>
@endsection


