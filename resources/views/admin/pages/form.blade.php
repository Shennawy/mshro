<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">إسم الصفحة </label>
    <div class="col-lg-5{{ $errors->has('pName') ? ' has-error' : '' }}">
        {!! Form::text("pName", null ,['class'=>'form-control m-input']) !!}
        @if ($errors->has('pName'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('pName') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">اسم رابط الصفحه </label>
    <div class="col-lg-5{{ $errors->has('pLink') ? ' has-error' : '' }}">
        {!! Form::text("pLink", null ,['class'=>'form-control m-input']) !!}
        @if ($errors->has('pLink'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('pLink') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">مكان الصفحة </label>
    <div class="col-lg-5{{ $errors->has('type') ? ' has-error' : '' }}">
        {!! Form::select("type",parentPages(),null,['class'=>'form-control m-input']) !!}
    @if ($errors->has('type'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('type') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">حالة الصفحة </label>
    <div class="col-lg-5{{ $errors->has('active') ? ' has-error' : '' }}">
        {!! Form::select("active", ['1'=>'مفعله','0'=>'غير مفعله'] ,null ,['class'=>'form-control m-input']) !!}
    @if ($errors->has('active'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('active') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row{{ $errors->has('content') ? ' has-error' : '' }}">
    <label class="col-lg-1 col-form-label">المحتوى </label>
    <div class="col-lg-11">
        {!! Form::textarea("content", null ,['class'=>'form-control m-input']) !!}
        @if ($errors->has('content'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('content') }}</strong>
            </span>
        @endif
    </div>
</div>