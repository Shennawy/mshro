<li class="m-menu__item">
    <a href="{{url('admincp')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-line-graph"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">إحصائيات عامة</span>
                {{--
                <span class="m-menu__link-badge">
                    <span class="m-badge m-badge--danger">2</span>
                </span>
                --}}
            </span>
        </span>
    </a>
</li>
@can('setting-list')
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="javascript:;" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-settings"></i>
            <span class="m-menu__link-text">إعدادت الموقع</span>
            <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
            <span class="m-menu__arrow"></span>
            <ul class="m-menu__subnav">
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/settings')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">التحكم بإعدادات الموقع</span>
                    </a>
                </li>
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/sliders')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">التحكم بالسلايدرات</span>
                    </a>
                </li>
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/settings/siteMechanism')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">آلية عمل الموقع</span>
                    </a>
                </li>
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/settings/siteServices')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">لماذا تختارنا</span>
                    </a>
                </li>
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/settings/siteAds')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">الإعلانات بالموقع</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>
@endcan
@can('role-list')
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="javascript:;" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-customer"></i>
            <span class="m-menu__link-text">نظام الصلاحيات</span>
            <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
            <span class="m-menu__arrow"></span>
            <ul class="m-menu__subnav">
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/roles')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">عرض المجموعات</span>
                    </a>
                </li>
                @can('role-create')
                    <li class="m-menu__item" aria-haspopup="true">
                        <a href="{{url('admincp/roles/create')}}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">إضافه مجموعه جديده</span>
                        </a>
                    </li>
                @endcan
            </ul>
        </div>
    </li>
@endcan
@can('user-list')
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="javascript:;" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-users"></i>
            <span class="m-menu__link-text">الأعضاء</span>
            <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
            <span class="m-menu__arrow"></span>
            <ul class="m-menu__subnav">
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/users')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">الأعضاء</span>
                    </a>
                </li>
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/users/archive-users')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">الأرشيف</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>
@endcan
@can('admin-list')
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="javascript:;" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-user-settings"></i>
            <span class="m-menu__link-text">المديرين</span>
            <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
            <span class="m-menu__arrow"></span>
            <ul class="m-menu__subnav">
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/admins')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">المديرين</span>
                    </a>
                </li>
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/admins/archive-users')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">الأرشيف</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>
@endcan
@can('admin-list')
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="javascript:;" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-interface-11"></i>
            <span class="m-menu__link-text">نظام الصفحات</span>
            <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
            <span class="m-menu__arrow"></span>
            <ul class="m-menu__subnav">
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/pages')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">عرض الصفحات</span>
                    </a>
                </li>
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/pages/create')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">إضافة صفحة جديده</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>
@endcan
@can('admin-list')
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="javascript:;" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-interface-11"></i>
            <span class="m-menu__link-text">نظام الاعلانات</span>
            <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
            <span class="m-menu__arrow"></span>
            <ul class="m-menu__subnav">
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/posts')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">عرض جميع الاعلانات</span>
                    </a>
                </li>
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/posts?status=0')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">عرض اعلانات تحت المراجعة</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>
@endcan
@can('admin-list')
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="javascript:;" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-list"></i>
            <span class="m-menu__link-text">نظام التدوينات</span>
            <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
            <span class="m-menu__arrow"></span>
            <ul class="m-menu__subnav">
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/blogs')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">عرض التدوينات</span>
                    </a>
                </li>
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('admincp/blogs/create')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">إضافة تدوينه جديده</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>
@endcan
@can('contact-list')
    <li class="m-menu__item">
        <a href="{{url('admincp/contacts')}}" class="m-menu__link ">
            <i class="m-menu__link-icon flaticon-email"></i>
            <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الرسائل والاستفسارات</span>
                {{--
                <span class="m-menu__link-badge">
                    <span class="m-badge m-badge--danger">2</span>
                </span>
                --}}
            </span>
        </span>
        </a>
    </li>
@endcan
@can('menu-list')
    <li class="m-menu__item">
        <a href="{{url('admincp/menus')}}" class="m-menu__link ">
            <i class="m-menu__link-icon flaticon-signs-1"></i>
            <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">نظام القوائم</span>
            </span>
        </span>
        </a>
    </li>
@endcan
