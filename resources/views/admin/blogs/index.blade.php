@extends('admin.layouts.app')
@section('title','نظام التدوينات')
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('admincp')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="#" class="m-menu__link">
            <span class="m-menu__link-text">نظام التدوينات</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/admincp/blogs/create')}}" class="m-menu__link">
            <span class="m-menu__link-text">إضافة تدوينة جديده</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('content')
    <!-- Main content -->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            التحكم بالتدوينات
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable">
                    <thead>
                    <tr>
                        <th>م</th>
                        <th style="min-width:90px">عنوان التدوينه</th>
                        <th style="width:500px">نص التدوينة</th>
                        <th>الأدوات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($blogs as $blog)
                        <tr>
                            <td>{{$num++}}</td>
                            <td>{{$blog->title}}</td>
                            <td>{{ str_limit(strip_tags($blog->body),120,'.....') }}</td>
                            <td>
                                @can('blog-edit')
                                    <a href="{{url('/admincp/blogs/'.$blog->id.'/edit')}}"
                                       class="btn btn-success"><i class="fa fa-edit"></i> تعديل </a>
                                @endcan
                                @can('blog-delete')
                                    <a href="#" onclick="$('.del-blog{{$blog->id}}').submit()"
                                       class="btn btn-danger"><i class="fa fa-trash-o"></i> حذف </a>
                                    <form class="hidden del-blog{{$blog->id}}" method="post"
                                          action="{{url('admincp/blogs/'.$blog->id)}}"> @csrf @method('delete') </form>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
@endsection
