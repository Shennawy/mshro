<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">عنوان التدوينة : </label>
    <div class="col-lg-3{{ $errors->has('title') ? ' has-danger' : '' }}">
        {!! Form::text('title',null,['class'=>'form-control m-input','required','autofocus','placeholder'=>"عنوان التدوينة"]) !!}
        @if ($errors->has('title'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">رفع صورة التدوينة</label>
    <div class="col-lg-{{!isset($blog) ? 5 : 3}}">
        {!! Form::file('image[]',array('class'=>'form-control m-input')) !!}
        @if ($errors->has('imageCount'))
            <span class="form-control-feedback" role="alert">
                    <strong>{{ $errors->first('imageCount') }}</strong>
                </span>
        @endif
    </div>
    <a data-toggle="modal" @if(!isset($blog)) class="d-none" @endif href="#large" class="btn btn-accent">صورة التدوينة</a>
</div>
<div class="form-group m-form__group row{{ $errors->has('body') ? ' has-error' : '' }}">
    <label class="col-lg-1 col-form-label">المحتوى</label>
    <!-- <div class="clearfix"></div> -->
    <div class="col-lg-11">
        {!! Form::textarea("body", null ,['class'=>'form-control m-input']) !!}
        @if ($errors->has('body'))
            <span class="form-control-feedback" role="alert">
                    <strong>{{ $errors->first('body') }}</strong>
                </span>
        @endif
    </div>
</div>
@if(isset($blog))
    <div class="modal fade modal-scroll" id="large" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">صورة الخبر</h4>
                </div>
                <div class="modal-body">
                    @foreach(\App\File::where('id',$blog->image)->get() as $image)
                        <img class="img-responsive img-block" width="100%" src="{{$image->url}}">
                        <hr/>
                    @endforeach
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endif
