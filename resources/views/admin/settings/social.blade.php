@extends('admin.layouts.app')

@section('title','إعدادات الموقع')
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('admincp')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="#" class="m-menu__link">
            <span class="m-menu__link-text">روابط السوشيال ميديا</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
@endsection

@section('content')
    <!-- Main content -->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        تعديل روابط السوشيال ميديا
                    </h3>
                </div>
            </div>
        </div>
        <form class="form-horizontal" role="form" method="POST" action="{{ url('admincp/settings') }}"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="m-portlet__body">

            @foreach($settings as $setting )
                @if($setting->type == 30)
                    <div class="form-group{{ $errors->has('$setting->name') ? ' has-error' : '' }}">
                        <label class="col-md-2 col-sm-2 col-xs-12 control-label">{{$setting->slug}}</label>
                        <!-- <div class="clearfix"></div> -->
                        <div class="col-md-9 col-sm-10 col-xs-12{{ $errors->has('color') ? ' has-error' : '' }}">
                            <div class="input-group">
                        	            <span class="input-group-addon">
                        	                @php $checkbOX = $settings->where('type',34)->where('name',$setting->name.'Act')->first() @endphp
                        	                <input type="hidden" name="{{$checkbOX->name}}" value="0"/>
                                            <label class="m-checkbox">
                            	                {!! Form::checkbox($checkbOX->name,1,$checkbOX->value == 1 ? 'checked' : false) !!}
                                                <span></span>
                                            </label>
                        	                <span></span>
                        	            </span>
                                {!! Form::text($setting->name, $setting->value ,['class'=>'form-control m-input']) !!}
                            </div>
                        </div>
                    </div>
                @elseif($setting->type == 33)
                    <div class="form-group{{ $errors->has('$setting->name') ? ' has-error' : '' }}">
                        <label class="col-md-2 control-label">{{$setting->slug}}</label>
                        <div class="col-md-9">
                            <div class="input-group">
                        	            <span class="input-group-addon">
                        	            <i class="icon-settings"></i>
                        	            </span>
                                {!! Form::select("$setting->name",['1'=>'تفعيل','0'=>'تعطيل'],$setting->value,['class'=>'form-control']) !!}
                            </div>
                            @if ($errors->has('$setting->name'))
                                <span class="help-block">
                        	                <strong>{{ $errors->first('$setting->name') }}</strong>
                        	            </span>
                            @endif
                        </div>
                    </div>
                @endif
            @endforeach
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions--solid">
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-6">
                                <button type="submit" class="btn btn-success">تعديل</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

<!-- footer -->
@section('footer')

@endsection