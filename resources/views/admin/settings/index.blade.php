@extends('admin.layouts.app')

@section('title','إعدادات الموقع')
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('admincp')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="{{url('admincp/settings')}}" class="m-menu__link">
            <span class="m-menu__link-text">إعدادات الموقع</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
@endsection

@section('header')
    {!! Html::style('admin/custom/tagsinput/bootstrap-tagsinput.css') !!}
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        تعديل إعدادات الموقع
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/admincp/settings') }}" enctype="multipart/form-data">
            @csrf
            <div class="m-portlet__body">
                @foreach($settings as $setting )
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">{{$setting->slug}} : </label>
                        <div class="col-lg-6{{ $errors->has('name') ? ' has-danger' : '' }}">
                            @if($setting->type == 0)
                                @if($setting->name == 'KeyWords')
                                    {!! Form::text($setting->name, $setting->value ,['class'=>'form-control m-input','data-role'=>'tagsinput']) !!}
                                @else
                                    {!! Form::text($setting->name, $setting->value ,['class'=>'form-control m-input']) !!}
                                @endif
                            @elseif($setting->type == 1)
                                <span class="btn default blue-stripe fileinput-button form-control">
                                    @if($setting->name != null)
                                        <img src="{{url('upload/logo/'.$setting->value)}}" width="100" height="20">
                                    @else
                                        <img src="{{url('upload/logo/no_image.png')}}" width="100" height="20">
                                    @endif
                                    <i class="fa fa-plus"></i>
                                    <span>أختر الملف ... </span>
                                        {!! Form::file($setting->name, array('multiple'=>false)) !!}
                                    </span>
                            @elseif($setting->type == 2)
                                {!! Form::textarea($setting->name, $setting->value ,['class'=>'form-control']) !!}
                            @elseif($setting->type == 3)
                                {!! Form::select("$setting->name",['0'=>'مغلق','1'=>'مفتوح'],$setting->value,['class'=>'form-control']) !!}
                            @elseif($setting->type == 8)
                                {!! Form::textarea($setting->name, $setting->value ,['class'=>'form-control','id'=>$setting->name]) !!}
                            @elseif($setting->type == 9)
                                {!! Form::text($setting->name, $setting->value ,['class'=>'form-control m-input','data-role'=>'tagsinput']) !!}
                            @endif
                            @if ($errors->has('name'))
                                <span class="form-control-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-success">تعديل</button>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        {!! Form::close() !!}
        <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    {!! Html::script('admin/custom/tagsinput/bootstrap-tagsinput.min.js') !!}
    <script type="text/javascript">

    </script>
@endsection

