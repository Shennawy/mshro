@extends('admin.layouts.app')
@section('title','نظام السلايدرات')
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('admincp')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="#" class="m-menu__link">
            <span class="m-menu__link-text">نظام السلايدرات</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/admincp/sliders/create')}}" class="m-menu__link">
            <span class="m-menu__link-text">إضافة سلايدر جديد</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('content')
    <!-- Main content -->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            التحكم بالسلايدرات
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable">
                    <thead>
                    <tr>
                        <th>م</th>
                        <th style="min-width:90px">عنوان السلايدر</th>
                        <th>الأدوات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sliders as $slider)
                        <tr>
                            <td>{{$num++}}</td>
                            <td>{{$slider->title}}</td>
                            <td>
                                    <a href="{{url('/admincp/sliders/'.$slider->id.'/edit')}}"
                                       class="btn btn-success"><i class="fa fa-edit"></i> تعديل </a>
                                    <a href="#" onclick="$('.del-slider{{$slider->id}}').submit()"
                                       class="btn btn-danger"><i class="fa fa-trash-o"></i> حذف </a>
                                    <form class="hidden del-slider{{$slider->id}}" method="post"
                                          action="{{url('admincp/sliders/'.$slider->id)}}"> @csrf @method('delete') </form>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
@endsection
