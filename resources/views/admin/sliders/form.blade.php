<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">عنوان السلايدر : </label>
    <div class="col-lg-3{{ $errors->has('title') ? ' has-danger' : '' }}">
        {!! Form::text('title',null,['class'=>'form-control m-input','required','autofocus','placeholder'=>"عنوان السلايدر"]) !!}
        @if ($errors->has('title'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">رفع صورة السلايدر</label>
    <div class="col-lg-{{!isset($slider) ? 5 : 3}}">
        {!! Form::file('image',array('class'=>'form-control m-input')) !!}
        @if ($errors->has('imageCount'))
            <span class="form-control-feedback" role="alert">
                    <strong>{{ $errors->first('imageCount') }}</strong>
                </span>
        @endif
    </div>
    <a data-toggle="modal" @if(!isset($slider)) class="d-none" @endif href="#large" class="btn btn-accent">صورة السلايدر</a>
</div>
@if(isset($slider))
    <div class="modal fade modal-scroll" id="large" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">صورة السلايدر</h4>
                </div>
                <div class="modal-body">
                        <img class="img-responsive img-block" width="100%"
                             src="{{Request::root()}}/uploads/sliders/{{$slider->image}}">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endif
