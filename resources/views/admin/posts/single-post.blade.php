@extends('admin.layouts.app')

@section('title')
	تعديل : {{$user->name}}
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item">
		<a href="{{url('admincp/posts')}}" class="m-menu__link">
			<span class="m-menu__link-text">التحكم بالإعلانات</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="javascript:;" class="m-menu__link">
			<span class="m-menu__link-text">تعديل : {{$post->title}}</span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection

@section('header')
@endsection

@section('content')
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__body">
			<ul class="nav nav-tabs  m-tabs-line m-tabs-line--2x m-tabs-line--success" role="tablist">
				<li class="nav-item m-tabs__item">
					<a class="nav-link m-tabs__link active" data-toggle="tab" href="#up-update" role="tab">تعديل بيانات {{$post->title}}</a>
				</li>
				<li class="nav-item m-tabs__item">
					<a class="nav-link m-tabs__link" data-toggle="tab" href="#messages" role="tab">العروض</a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="up-update" role="tabpanel">
					<!--begin::Form-->
          <form method="{{url('admincp/posts/save')}}" class="m-form m-form--fit m-form--label-align-right">
            {{ csrf_field() }}
            <input type="input" name="id" value="{{$post->id}}">
					{{-- {!! Form::model($post,['route' => ['users.update' , $user->id],'method'=> 'PATCH','class'=>'m-form m-form--fit m-form--label-align-right']) !!} --}}
					<div class="m-portlet__body">
						@include('admin.posts.form')
					</div>
					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<button type="submit" class="btn btn-success">تعديل</button>
								</div>
							</div>
						</div>
					</div>
        </form>
					{{-- {!! Form::close() !!} --}}
					<!--end::Form-->
				</div>
				<div class="tab-pane" id="messages" role="tabpanel">

				</div>
			</div>
		</div>
	</div>
	<!--end::Portlet-->
@endsection
@section('footer')
@endsection
