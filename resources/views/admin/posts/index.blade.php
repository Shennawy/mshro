@extends('admin.layouts.app')

@section('title','عرض الأعضاء')

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('admincp')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="javascript:;" class="m-menu__link">
            <span class="m-menu__link-text">التحكم بالاعلانات</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
@endsection
@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('content')
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            التحكم بالاعلانات
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>عنوان الإعلان</th>
                        <th>مالك الإعلان</th>
                        <th>الأدوات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr class="debTr">
                            <td>{{$post->id}}</td>
                            <td>{{$post->title}}</td>
                            <td>
                              <a href="{{url('admincp/users/'.$post->User->id.'/edit')}}">{{$post->User->name}}</a></td>
                            <td>
                                @if($post->status == 0)
                                  <a href="{{url('admincp/posts/'.$post->id.'/active')}}"
                                     class="btn btn-success m-btn">تفعيل</a>
                                   @endif
                                    <a href="{{url('admincp/posts/'.$post->id)}}"
                                       class="btn btn-brand m-btn editDeps">تعديل</a>


                                        <a href="{{url('admincp/posts/'.$post->id.'/force')}}"
                                           class="btn btn-danger m-btn">حذف نهائى</a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
@endsection
@section('footer')
    <!--begin::Page Vendors -->
    {!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}
    {!! Html::script('admin/custom/users/script.js') !!}
    <!--end::Page Vendors -->
@endsection
