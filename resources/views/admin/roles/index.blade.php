@extends('admin.layouts.app')

@section('title','التحكم بالصلاحيات')
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="javascript:;" class="m-menu__link">
			<span class="m-menu__link-text">التحكم بالصلاحيات</span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
	@can('role-create')
	<li class="m-menu__item">
		<a href="{{url('admincp/roles/create')}}" class="m-menu__link">
			<span class="m-menu__link-text">إضافة مجموعه جديده</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	@endcan
@endsection

@section('content')
	<!-- END: Subheader -->
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
						عرض المجموعات
						</h3>
					</div>
				</div>
			</div>
			<div class="m-portlet__body">

				<!--begin: Datatable -->
				<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
					<thead>
						<tr>
							<th>#</th>
							<th>إسم المجموعه</th>
							<th>الأدوات</th>
						</tr>
					</thead>
					<tbody>
		            	@php $i = 1 @endphp
		                @foreach ($roles as $role)
		                <tr>
		                    <td>{{ $i++ }}</td>
		                    <td>{{ $role->slug }}</td>
		                    <td>
	                            @can('role-edit')
	                            <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">تعديل</a>
	                            @endcan
	                            @can('role-delete')
	                            @if(!in_array($role->id,[1]))
	                            {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
	                                {!! Form::submit('حذف', ['class' => 'btn btn-danger']) !!}
	                            {!! Form::close() !!}
		                    	@endif
		                    	@endcan
		                    </td>
		                </tr>
		                @endforeach                  
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
@endsection
@section('footer')
@endsection

