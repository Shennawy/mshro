<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">إسم المجموعه بالعربيه : </label>
    <div class="col-lg-{{isset($role) ? '10' : '4' }}{{ $errors->has('slug') ? ' has-danger' : '' }}">
        {!! Form::text('slug', null, ['placeholder' => 'إسم المجموعه بالعربيه','class' => 'form-control m-input','required']) !!}
        @if(isset($role))
        {!! Form::hidden('name', $role->name) !!}
        @endif
        @if ($errors->has('slug'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('slug') }}</strong>
            </span>
        @endif
    </div>
    @if(!isset($role))
    <label class="col-lg-2 col-form-label">إسم المجموعه بالإنجليزيه : </label>
    <div class="col-lg-4{{ $errors->has('name') ? ' has-danger' : '' }}">
        {!! Form::text('name', null, array('placeholder' => 'إسم المجموعه بالإنجليزيه','class' => 'form-control')) !!}
        @if ($errors->has('name'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    @endif
</div>
<div class="m-form__group form-group row">
    <label class="col-lg-2 col-form-label">تحديد كل الصلاحيات : </label>
    <div class="col-lg-3 m-checkbox-list">
        <label class="m-checkbox" style="color: #f09">
            <input type="checkbox" class="slctAlGroups"> 
            تحديد الكل
            <span></span>
        </label>
    </div>
</div>
<div class="m-form__group form-group row {{ $errors->has('permission') ? ' has-danger' : '' }}">
    <label class="col-lg-2 col-form-label">صلاحيات المجموعه : 
        @if ($errors->has('permission'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('permission') }}</strong>
            </span>
        @endif
    </label>
	<?php $oldOrder = 1; $count = 0;?>
    @foreach($permission->pluck('orderBy')->unique() as $value)
    @if($count != 0 && $count % 3 == 0)
        <div class="col-lg-12 m-checkbox-list"><br></div>
        <div class="col-lg-2 m-checkbox-list"></div>
    @endif
    <div class="col-lg-3 m-checkbox-list">
		<label class="m-checkbox" style="color: #f09">
			<input type="checkbox" class="slctAlPerms"> 
			{{multipleLogs()[$value]}}
			<span></span>
		</label>
		@foreach($permission->where('orderBy',$value) as $value)
        <label class="m-checkbox">
            {{ Form::checkbox('permission[]', $value->id,isset($role) ? in_array($value->id, $rolePermissions) ? true : false : false, array('class' => 'name icheck')) }} {{ $value->slug }} 
			<span></span>
        </label>
		@endforeach
    </div>
    <?php $count++; ?>
    @endforeach
</div>
