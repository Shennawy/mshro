@extends('admin.layouts.app')

@section('title','عرض الأعضاء')

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('admincp')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="javascript:;" class="m-menu__link">
            <span class="m-menu__link-text">التحكم بالأعضاء</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
@endsection
@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('content')
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            التحكم بالأعضاء
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>الإسم</th>
                        <th>البريد الإلكترونى</th>
                        <th>الأدوات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr class="debTr">
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                @can('user-edit')
                                    <a href="{{url('admincp/users/'.$user->id.'/edit')}}"
                                       class="btn btn-brand m-btn editDeps">تعديل</a>
                                @endcan
                                @can('user-delete')
                                    @if(in_array('archive-users',Request::segments()))
                                        <a href="{{url('admincp/users/'.$user->id.'/restore')}}"
                                           class="btn btn-warning m-btn">استرجاع</a>
                                        <a href="{{url('admincp/users/'.$user->id.'/force')}}"
                                           class="btn btn-danger m-btn">حذف نهائى</a>
                                    @else
                                        <a href="{{url('admincp/users/'.$user->id)}}" data-id="{{$user->id}}"
                                           class="btn btn-danger m-btn delUser">نقل للأرشيف</a>
                                    @endif
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
@endsection
@section('footer')
    <!--begin::Page Vendors -->
    {!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}
    {!! Html::script('admin/custom/users/script.js') !!}
    <!--end::Page Vendors -->
@endsection

