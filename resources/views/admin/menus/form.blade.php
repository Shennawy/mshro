<div class="form-group m-form__group row {{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-lg-3 col-form-label">نوع الخانة </label>
    <div class="col-lg-9">
        {!! Form::select("status",menuStatus(),null,['class'=>'form-control']) !!}
        @if ($errors->has('status'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row {{ $errors->has('pageModel') ? ' has-error' : '' }}">
    <label class="col-lg-3 col-form-label">إختر الصفحة </label>
    <div class="col-lg-9">
        {!! Form::select("pageModel",getPagesToSelect(),null,['class'=>'form-control']) !!}
        @if ($errors->has('pageModel'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row {{ $errors->has('title') ? ' has-error' : '' }}">
    <label class="col-lg-3 col-form-label">النص فى القائمة </label>
    <div class="col-lg-9">
        {!! Form::text("title", isset($oneMenu) ? $oneMenu->title : null ,['class'=>'form-control','placeholder' => 'مثال ( سيارت )','required']) !!}
        @if ($errors->has('title'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row {{ $errors->has('link') ? ' has-error' : '' }}">
    <label class="col-lg-3 col-form-label">أكتب إسم القسم</label>
    <div class="col-lg-9">
        {!! Form::text("link", null ,['class'=>'form-control','placeholder' => 'مثال ( تويوتا ) أو رقم معرف القسم مثال ( 3 )']) !!}
        @if ($errors->has('link'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('link') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">
    <label class="col-lg-3 col-form-label">حالة الخانة</label>
    <div class="col-lg-4{{ $errors->has('mainMenu') ? ' has-error' : '' }}">
        <div class="m-checkbox-list">
            <label class="m-checkbox">
                <input type="checkbox"> قائمة رئيسية
                {!! Form::checkbox('mainMenu',null,isset($oneMenu) ? $oneMenu->parent_id == 0 ? ['checked'=>'true'] : '' : '') !!}
                <span></span>
            </label>
        </div>
        @if ($errors->has('mainMenu'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('mainMenu') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-lg-5">
        <div class="m-checkbox-list">
            <label class="m-checkbox">
                <input type="checkbox"> فتح الرابط في تاب جديدة
                {!! Form::checkbox('target') !!}
                <span></span>
            </label>
        </div>
        @if ($errors->has('target'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('target') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">
    <label class="col-lg-3 col-form-label {{ $errors->has('parent_id') ? ' has-error' : '' }}">تابعة لقائمة</label>
    <div class="col-lg-4">
        <?php
        $catsArray = \App\Menu::where('parent_id',0)->orderBy('order_by')->pluck('title','id')->toArray();
        foreach ($catsArray as $key => $value) {
            $catsArray[$key] = $value;
        }
        ?>
        {!! Form::select("parent_id",$catsArray,null,['class'=>'form-control']) !!}
        @if ($errors->has('parent_id'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('parent_id') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-3 col-form-label">الترتيب</label>
    <div class="col-lg-2{{ $errors->has('order_by') ? ' has-error' : '' }}">
        {!! Form::text("order_by", 1 ,['class'=>'form-control','placeholder' => 'الترتيب']) !!}
        @if ($errors->has('order_by'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('order_by') }}</strong>
            </span>
        @endif
    </div>
</div>
