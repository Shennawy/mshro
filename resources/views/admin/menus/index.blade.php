@extends('admin.layouts.app')
@section('title','التحكم بالقوائم')
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('admincp')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="#" class="m-menu__link">
            <span class="m-menu__link-text">نظام القوائم</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
@endsection

@section('header')
    <!-- DataTables -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    {!! Html::style('admin/custom/jquery-nestable/jquery.nestable-rtl.css') !!}
    <!-- END PAGE LEVEL STYLES -->
    <style type="text/css">
        .addMenuForm {
            position: sticky;
            top: 55px;
        }
    </style>
@endsection

@section('content')
    <div class="m-portlet">
        <div class="m-portlet__body">
            <ul class="nav nav-tabs  m-tabs-line m-tabs-line--2x m-tabs-line--success" role="tablist">
                @foreach(menusType() as $key => $tab)
                    <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link {{$key == 1 ? 'active' : '' }}" href="#tab_1_{{$key}}"
                           data-toggle="tab" role="tab">
                            {{$tab}}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach(menusType() as $key => $tab)
                    <div class="tab-pane {{$key == 1 ? 'active' : '' }} fontawesome-demo" id="tab_1_{{$key}}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="portlet light bordered">
                                    <div class="col-md-12">
                                        <div class="margin-bottom-10" id="nestable_list_menu">
                                            <!-- <button type="button" class="btn green btn-outline sbold uppercase" data-action="expand-all">فتح الكل</button> -->
                                            <button type="button" class="btn btn-outline-accent sbold uppercase"
                                                    data-action="expand-all"> فتح الكل <i class="fa fa-angle-down"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="portlet-body ">
                                        <div class="dd" id="nestable_list_1">
                                            <ol class="dd-list">
                                                @foreach($menus->where('parent_id',0)->sortBy('order_by') as $menu)
                                                    <li class="dd-item" data-id="{{$menu->id}}">
                                                        <div class="dd-handle"
                                                             data-parent="{{$menu->parent_id}}"> {{$menu->title}}
                                                            <span class="pull-left dd-nodrag">
                                                            <a href="{{url('admincp/menus/'.$menu->id.'/edit')}}"
                                                               class="editMenu"><i class="fa fa-edit"></i> تعديل </a>
                                                            <a href="{{url('admincp/menus/'.$menu->id)}}"
                                                               data-token="{{ csrf_token() }}" class="delMenu"><i
                                                                        class="fa fa-trash-o"></i> حذف </a>
                                                        </span>
                                                        </div>

                                                        @if($menus->where('parent_id',$menu->id)->count())
                                                            <ol class="dd-list">
                                                                @foreach($menus->where('parent_id',$menu->id)->sortBy('order_by') as $subMenu)
                                                                    <li class="dd-item" data-id="{{$subMenu->id}}">
                                                                        <div class="dd-handle"> {{$subMenu->title}}
                                                                            <span class="pull-left dd-nodrag">
                                                                    <a href="{{url('admincp/menus/'.$subMenu->id.'/edit')}}"
                                                                       class="editMenu"><i class="fa fa-edit"></i> تعديل </a>
                                                                    <a href="{{url('admincp/menus/'.$subMenu->id)}}"
                                                                       data-token="{{ csrf_token() }}"
                                                                       class="delMenu"><i class="fa fa-trash-o"></i> حذف </a>
                                                                </span>
                                                                        </div>
                                                                        @if($menus->where('parent_id',$subMenu->id)->count())
                                                                            <ol class="dd-list">
                                                                                @foreach($menus->where('parent_id',$subMenu->id)->sortBy('order_by') as $subMenu2)
                                                                                    <li class="dd-item"
                                                                                        data-id="{{$subMenu2->id}}">
                                                                                        <div class="dd-handle"> {{$subMenu2->title}}
                                                                                            <span class="pull-left dd-nodrag">
                                                                        <a href="{{url('admincp/menus/'.$subMenu2->id.'/edit')}}"
                                                                           class="editMenu"><i class="fa fa-edit"></i> تعديل </a>
                                                                        <a href="{{url('admincp/menus/'.$subMenu2->id)}}"
                                                                           data-token="{{ csrf_token() }}"
                                                                           class="delMenu"><i class="fa fa-trash-o"></i> حذف </a>
                                                                    </span>
                                                                                        </div>
                                                                                    </li>
                                                                                @endforeach
                                                                            </ol>
                                                                        @endif
                                                                    </li>
                                                                @endforeach
                                                            </ol>
                                                        @endif
                                                    </li>
                                                @endforeach

                                            </ol>
                                        </div>
                                    </div>
                                    <br>
                                    <form action="{{url('admincp/menus/order')}}" method="post">
                                        {{csrf_field()}}
                                        <textarea id="nestable_list_1_output" name="order" class="d-none"></textarea>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-9">
                                                    <button type="submit" class="btn btn-success"> حفظ التعديلات</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-6 addMenuForm">
                                <div class="m-portlet light bordered" id="MenuForm">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <span class="m-portlet__head-icon m--hide">
                                                    <i class="la la-gear"></i>
                                                </span>
                                                <h3 class="m-portlet__head-text">
                                                    إضافة خانه جديده للقائمة
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                    <form role="form" class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('/admincp/menus') }}">
                                        {{ csrf_field() }}
                                        <div class="m-portlet__body">
                                            @include('admin.menus.form')
                                        </div>
                                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                            <div class="m-form__actions m-form__actions--solid">
                                                <div class="row">
                                                    <div class="col-lg-2"></div>
                                                    <div class="col-lg-6">
                                                        <button type="submit" class="btn btn-success">إضافة</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
<!-- footer -->
@section('footer')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    {!! Html::script('admin/custom/jquery-nestable/jquery.nestable-rtl.js') !!}
    {!! Html::script('admin/custom/jquery-nestable/script.js') !!}
    <!-- END PAGE LEVEL PLUGINS -->
    <script type="text/javascript">
        $(document).on("click", ".delMenu", function (e) {
            e.preventDefault();
            var a = $(this), token = $(this).data('token'),
                id = $(this).attr('bankId'),
                route = $(this).attr('href');
            $.ajax({
                url: route,
                type: 'post',
                data: {_method: 'delete', _token: token},
                success: function (data) {
                    a.closest('li').remove();
                    if (data == "done") {
                        toastr.options.timeOut = '6000';
                        toastr.options.positionClass = 'toast-bottom-left';
                        Command: toastr["success"]("تم الحذف بنجاح")
                    }
                }
            });
            return;
        });
    </script>
@endsection


