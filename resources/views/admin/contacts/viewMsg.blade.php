@extends('admin.layouts.app')
@section('title','عرض رساله')
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('admincp')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="#" class="m-menu__link">
            <span class="m-menu__link-text">الرسائل والاستفسارات</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
@endsection
@section('header')
    <style>
        .m-invoice-2 .m-invoice__wrapper .m-invoice__head .m-invoice__container .m-invoice__items {
            border-top: 0;
            padding: 5px 0 15px 0;
        }
        .m-invoice__subtitle {
            font-size: 14px;
        }
    </style>
@endsection
@section('content')
    <div class="col-xl-12">
        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{str_limit($contact->subject,60)}}
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--right m-tabs-line-danger" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_portlet_tab_1_1"
                               role="tab">
                                عرض الرساله
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_portlet_tab_1_2" role="tab">
                                الرد على الرساله
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content m-invoice-2">
                    <div class="tab-pane active m-invoice__wrapper" id="m_portlet_tab_1_1">
                        <div class="m-invoice__head"
                             style="background-image: url(../../assets/app/media/img//logos/bg-6.jpg);">
                            <div class="m-invoice__container m-invoice__container--centered">
                                <div class="m-invoice__items">
                                    <div class="m-invoice__item">
                                        <span class="m-invoice__subtitle">رساله رقم : </span>
                                        <span class="m-invoice__text">{{$contact->id}} -> {{contactType()[$contact->type]}}</span>
                                    </div>
                                    <div class="m-invoice__item">
                                        <span class="m-invoice__subtitle">بيانات الراسل</span>
                                        <span class="m-invoice__text">الإسم :{{$contact->name}}</span>
                                        <span class="m-invoice__text">رقم الجوال :{{$contact->phone}}</span>
                                        <span class="m-invoice__text">البريد الإلكترونى :{{$contact->email}}</span>
                                    </div>
                                    <div class="m-invoice__item">
                                        <span class="m-invoice__subtitle">تاريخ الإرسال</span>
                                        <span class="m-invoice__text">{{$contact->created_at}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-form__seperator m-form__seperator--dashed"></div>
                        <div class="m-portlet">
                            <div class="m-portlet__body">
                                {{$contact->body}}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="m_portlet_tab_1_2">
                        <div class="m-portlet">
                            <!--begin::Form-->
                            <form action="{{url('admincp/contactsReplay')}}" method="POST"
                                  class="m-form m-form--fit m-form--label-align-right">
                                @csrf
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label class="col-lg-1 col-form-label">البريد الإلكترونى </label>
                                        <div class="col-lg-5{{ $errors->has('email') ? ' has-error' : '' }}">
                                            {!! Form::text("email", $contact->email ,['class'=>'form-control m-input']) !!}
                                            @if ($errors->has('email'))
                                                <span class="form-control-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row{{ $errors->has('body') ? ' has-error' : '' }}">
                                        <label class="col-lg-1 col-form-label">المحتوى </label>
                                        <div class="col-lg-11">
                                            {!! Form::textarea("body", NULL ,['class'=>'form-control m-input']) !!}
                                            @if ($errors->has('body'))
                                                <span class="form-control-feedback" role="alert">
                                                    <strong>{{ $errors->first('body') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                    <div class="m-form__actions m-form__actions--solid">
                                        <div class="row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-6">
                                                <button type="submit" class="btn btn-success">إرسال</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Portlet-->
    </div>
@endsection

<!-- footer -->
@section('footer')
    <script type="text/javascript">
    </script>
@endsection

