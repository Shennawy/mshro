@extends('admin.layouts.app')
@section('title','رسائل والاستفسارات')
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('admincp')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="#" class="m-menu__link">
            <span class="m-menu__link-text">عرض المراسلات</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
@endsection
@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection
@section('content')
    <div class="m-portlet">
        <div class="m-portlet__body">
            <ul class="nav nav-tabs  m-tabs-line m-tabs-line--2x m-tabs-line--success" role="tablist">
                @foreach(contactType() as $key => $tab)
                    <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link {{$key == 1 ? 'active' : '' }}" href="#tab_1_{{$key}}"
                           data-toggle="tab" role="tab">
                            {{$tab}}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach(contactType() as $key => $tab)
                    <div class="tab-pane {{$key == 1 ? 'active' : '' }} fontawesome-demo" id="tab_1_{{$key}}"
                         role="tabpanel">
                        <div class="portlet-body">
                            <table class="table table-striped table-hover table-bordered contact-table">
                                <thead>
                                <tr>
                                    <th style="width:20px !important">رقم الرساله</th>
                                    <th style="width:30px !important">البريد الإلكترونى</th>
                                    <th style="width:150px !important">العنوان</th>
                                    <th style="width:300px !important">المحتوى</th>
                                    <th>التاريخ</th>
                                    <th>الحاله</th>
                                    <th>الادوات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($contacts as $contact)
                                    @if($contact->type == $key)
                                        <tr>
                                            <td>{{$contact->id}}</td>
                                            <td>{{$contact->email}}</td>
                                            <td>{{str_limit($contact->subject,40)}}</td>
                                            <td>{{str_limit($contact->body,90)}}</td>
                                            <td> منذ {{ timeToStr(strtotime($contact->created_at))}}</td>
                                            <td> @if($contact->active == 0) مقرؤة @else غير مقرؤة @endif</td>
                                            <td>
                                                <a href="{{url('admincp/contacts/'.$contact->id)}}"
                                                   class="btn btn-success"><i class="fa fa-eye"></i> عرض </a>
                                                <a href="{{url('admincp/contacts/'.$contact->id)}}"
                                                   data-token="{{ csrf_token() }}" class="btn btn-danger del_msg"><i
                                                            class="fa fa-trash-o"></i> حذف </a>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('footer')
    {!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}

    <script type="text/javascript">
        var table = $('.contact-table');
        // begin first table
        table.DataTable({
            language: {
                aria: {
                    sortAscending: ": ترتيب تصاعدى",
                    sortDescending: ": ترتيب تنازلى"
                },
                emptyTable: "لا توجد اى بيانات متاحه",
                info: "إظهار _START_ إلى _END_ من _TOTAL_ حقل",
                infoEmpty: "لا توجد حقول",
                infoFiltered: "( الإجمالى _MAX_ حقل )",
                lengthMenu: "عدد الحقول : _MENU_",
                search: " بحث برقم الرساله :",
                zeroRecords: "لا توجد نتائج "
            },

            responsive: true,
            order: [[0, "desc"]],
            lengthMenu: [[10, 20, 30, 50, -1], [10, 20, 30, 50, "الكل"]],
            pageLength: 10,
            columnDefs: [{"targets": [1, 2, 3, 4, 5], "searchable": false}, {"targets": [5], "orderable": false}]
        });

        // Delete Messages
        $('.del_msg').on('click', function (e) {
            e.preventDefault();
            var url = $(this).attr("href"), token = $(this).data('token'), a = $(this);
            $.post(url, {_method: 'delete', _token: token}, function (data) {
                //success data
                a.closest('tr').hide();
                if (data == "empty") {
                    toastr.options.timeOut = '6000';
                    toastr.options.positionClass = 'toast-bottom--center';
                    Command: toastr["info"]("تم حذف الرساله بنجاح")
                    $('#sample_1 tbody').append("<tr class='odd'><td valign='top' colspan='6' class='dataTables_empty'>لا توجد اى بيانات متاحه</td></tr>");
                } else if (data == "done") {
                    toastr.options.timeOut = '6000';
                    toastr.options.positionClass = 'toast-bottom--center';
                    Command: toastr["info"]("تم حذف الرساله بنجاح")
                } else if (data == "error") {
                    toastr.options.timeOut = '6000';
                    toastr.options.positionClass = 'toast-bottom--center';
                    Command: toastr["error"]("لم يتم العثور على الرساله")
                }
            });
        });

    </script>
@endsection