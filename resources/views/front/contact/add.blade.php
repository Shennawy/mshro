@extends('front.layouts.master')
@section('title','إتصل بنا | ')
@section('header')
@endsection
@section('content')
    <!--start pages-head ================-->
    <section class="pages-head">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="white-title sec-head center-sec wow fadeIn"><span>أتصل بنا </span></h1>
                    <p class="white-prg wow fadeIn">نبذه مختصره عن التسجيل فى موقع مشروع نت .. نبذه مختصره عن التسجيل فى
                        موقع مشروع نت .. </p>
                </div>
            </div>
        </div>
    </section>
    <!--end pages-head-->

    <!--start login-pg ================-->
    <section class="services login-pg text-right marg-sec">
        <div class="container">
            <div class="row">
                <!--start service-grid-list-->
                <div class="col-lg-5 col-md-12  wow fadeIn">
                    <h2 class="sec-head wow fadeInDown"><span>معلومات التواصل</span></h2>
                    <!--start service-div-list-->
                    <div class="login-list contact-list wow fadeIn">
                        <ul class="list-unstyled">
                            <li><i class="fa fa-phone"></i>
                                <div class="c-list">
                                    @foreach(explode(',', getSettings('phones')) as $phone)
                                    <span>{{$phone}}</span>
                                    @endforeach
                                </div>
                            </li>
                            <li><a href="mailto:Mail@mail.com"><i class="fa fa-envelope"></i>{{getSettings('email')}}
                                </a></li>
                        </ul>
                    </div>
                    <!--end service-div-list-->

                </div>
                <!--end service-grid-list-->

                <!--start login-form-grid-->
                <div class="col-lg-7 col-md-12 login-form-grid">
                    <div class="login-form-div">
                        <div class="login-form-div">
                            <form method="post" action="{{url('contact')}}" class="needs-validation icons-form row" novalidate>
                                @csrf
                                <div class="form-group  col-12 wow fadeIn">
                                    <i class="fa fa-lock login-form-icon"></i>
                                    <input type="text" class="form-control" name="name" value="{{old('name')}}" id="name_input3"
                                           placeholder="الاسم بالكامل" required>
                                    <div class="invalid-feedback">
                                        من فضلك أدخل الإسم
                                    </div>
                                </div>


                                <div class="form-group  col-12 wow fadeIn">
                                    <i class="fa fa-envelope login-form-icon"></i>
                                    <input type="email" class="form-control" id="email_input1"
                                           aria-describedby="emailHelp" name="email" value="{{old('email')}}" placeholder=" البريد الالكتروني" required>
                                    <div class="invalid-feedback">
                                        من فضلك أدخل بريد الكتروني صحيح
                                    </div>
                                </div>


                                <div class="form-group  col-12 wow fadeIn">
                                    <i class="fa fa-phone login-form-icon"></i>
                                    <input type="text" class="form-control" id="phone" name="phone" value="{{old('phone')}}" minlength="10" maxlength="14"
                                           required>
                                    <div class="invalid-feedback">
                                        من فضلك أدخل رقم الهاتف
                                    </div>
                                </div>

                                <div class="form-group  col-12 wow fadeIn">
                                    <i class="fa fa-edit login-form-icon"></i>
                                    <input type="text" class="form-control" id="subject_input1"
                                           placeholder="عنوان الرساله" name="subject" value="{{old('subject')}}" required>
                                    <div class="invalid-feedback">
                                        من فضلك أدخل عنوان الرساله
                                    </div>
                                </div>

                                <div class="form-group  col-12 wow fadeIn">
                                    <i class="fa fa-envelope login-form-icon"></i>
                                    <textarea class="form-control" name="body" placeholder="الرساله" required>{{old('body')}}</textarea>
                                    <div class="invalid-feedback">
                                        من فضلك أدخل نص الرساله
                                    </div>
                                </div>


                                <div class="form-group submit-form-group two-btns col-12 wow fadeIn">
                                    <button type="submit" class="blue_btn custom_btn">ارسال الرساله</button>
                                    <button type="reset" class="blue_btn no-bord-btn custom_btn">افراغ الحقول</button>

                                </div>

                            </form>
                        </div>

                    </div>
                </div>
                <!--end login-form-grid-->
            </div>
        </div>
    </section>
    <!--end login-pg-->
    @include('front.components.join-us')

@endsection
@section('footer')
@endsection