<!DOCTYPE html>
<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates

  and open the template in the editor.
-->
<html lang="ar">
<head>
    <title>@yield('title') {{getSettings()}}</title>
    <!-- Meta tags ================ -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="author" content=""/>
    <meta name="csrf-token" content="{{csrf_token()}}"/>

    @if(Auth::user())
      @php
        if(!Auth::user()->Profile)
          Auth::user()->createProfile();
      @endphp
    <meta id="profile-pic" value="{{Auth::user()->Profile->image}}">
  @endif
    <!-- Style sheet ================ -->
    <link rel="stylesheet" href="{{url('/front/css/bootstrap.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('/front/css/bootstrap-rtl.min.css')}}" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{url('/front/css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="{{url('/front/css/owl.carousel.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('/front/css/owl.theme.default.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('/front/css/animate.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('/front/css/header.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('/front/css/footer.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('/front/css/keyframes.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('/front/css/style.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('/front/colors/blue.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('/front/css/responsive.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('/front/css/backendCustom.css')}}" type="text/css"/>
    {!! Html::style('admin/custom/toastr/toastr-rtl.min.css') !!}
    @yield('header')
</head>
<body>

@include('front.layouts.header')
@yield('content')
@include('front.layouts.footer')

<!-- Scripts ================-->
<script type="text/javascript" src="{{url('/front/js/html5shiv.min.js')}}"></script>
<script type="text/javascript" src="{{url('/front/js/respond.min.js')}}"></script>
<script type="text/javascript" src="{{url('/front/js/jquery-3.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{url('/front/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{url('/front/js/popper.min.js')}}"></script>
<script src="{{url('/front/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{url('/front/js/html5lightbox.js')}}"></script>
<script src="{{url('/front/js/wow.min.js')}}" type="text/javascript"></script>
<script src="{{url('/front/js/jPages.min.js')}}" type="text/javascript"></script>
<script src="{{url('/front/js/custom.js')}}" type="text/javascript"></script>
<script src="{{url('/front/js/backendCustom.js')}}" type="text/javascript"></script>
{!! Html::script('admin/custom/toastr/toastr.min.js') !!}

@yield('footer')
<div class="d-none">
    <span id="d-fmsg">{!! Session::get("flash_message") !!}</span>
</div>
</body>
</html>
