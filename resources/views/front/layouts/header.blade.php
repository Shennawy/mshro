<!--start header
             ================-->
             <div class="main-header">
    <header>
        <div class="container">
            <div class="row">
                <!--start logo-->
                <div class="col-xl-7 col-lg-6 col-md-4  col-sm-6  logo-grid text-right">
                    <div class="main-logo wow fadeIn">
                        <a href="{{url('/')}}" class="logo-link">
                            <div class="logo-img"></div>
                            <div class="side-logo">
                                <h4>{{getSettings()}}</h4>
                                {{getSettings('siteDesc-ar')}}
                            </div>
                        </a>
                    </div>
                </div>
                <!--end logo-->

                <!--start user-grid-->
                <div class="col-xl-5 col-lg-6 col-md-8  col-sm-6 nav-grid">
                    <a class="dark_btn custom_btn wow fadeIn" href="{{url('posts/add')}}" data-hover="أضف مشروعك الان">أضف مشروعك الان</a>

                    <!--start user-div-->
                    <div class="user-div wow fadeIn">
                        @if(Auth::check())
                        <div class="user_head">
                            <img src="/front/images/main/user.png" class="user-img" alt="user">
                            <div class="side-user">
                                <span class="user-name">{{Auth::user()->name}}</span>
                                <span class="user-job">{{Auth::user()->Profile->head_line ?? 'مستخدم'}}</span>
                            </div>
                        </div>
                        <div class="user-list">
                            <ul class="list-unstyled">
                                <li><a href="{{url('/profile')}}"><i class="fa fa-user-o"></i> الملف الشخصي</a></li>
                                <li><a href="{{url(Auth::user()->username.'/edit')}}"><i class="fa fa-pencil-square-o"></i> تعديل البيانات</a></li>
                                <li><a href="{{url('logout')}}"><i class="fa fa-power-off"></i>  خروج</a></li>
                            </ul>
                        </div>
                        @else
                        <div class="user_head">
                            <img src="/front/images/main/user.png" class="user-img" alt="user">
                            <div class="side-user">
                                <span class="user-name">مرحبا بك</span>
                                <span class="user-job">سجل معنا من هنا</span>
                            </div>
                        </div>
                        <div class="user-list">
                            <ul class="list-unstyled">
                                <li><a href="{{url('login')}}"><i class="fa fa-user-o"></i> تسجيل الدخول</a></li>
                                <li><a href="{{url('register')}}"><i class="fa fa-pencil-square-o"></i> تسجيل عضويه جديده</a></li>
                            </ul>
                        </div>
                        @endif
                    </div>
                    <!--end user-div-->

                    <!--start side-menu-->
                    <div class="side-menu">
                        <div class="nav-icon wow fadeIn"><span></span><span></span> <span></span></div>
                        <div class="side-navbar">
                            {{--
                            <!--start search-form-->
                            <form class="needs-validation" novalidate>
                                <div class="search-form">
                                    <div class="form-group">
                                        <label>بحث في الموقع</label>
                                        <input type="text" class="form-control" placeholder="بحث..." required />
                                        <div class="invalid-feedback">
                                            من فضلك أدخل كلامات بحث صحيحة
                                        </div>
                                        <button type="submit" class="form-btn"><i class="fa fa-search"></i></button>

                                    </div>
                                </div>
                            </form>
                            <!--end search-form-->
                            --}}
                            <nav class="nav-menu">
                                <ul class="list-unstyled">
                                    <li class="active"><a href="{{url('/')}}" data-hover="الرئيسية"><i class="fa fa-home"></i>الرئيسية</a></li>
                                    @foreach(\App\Menu::where('type',1)->where('parent_id',0)->orderBy('order_by','asc')->get() as $menu)
                                        <li class="{{\App\Menu::where('parent_id',$menu->id)->count() ? 'menu-item-has-child' : ''}}">
                                            @if($menu->status == 1)
                                            <a data-hover="{{$menu->title}}" href="http://{{$menu->link}}" {{$menu->target != 1 ? '' : 'target="_blank"'}}>
                                            @elseif($menu->status == 2)
                                            <a data-hover="{{$menu->title}}" href="#!">
                                            @elseif($menu->status == 3)
                                            <a data-hover="{{$menu->title}}" href="{{url($menu->link)}}" {{$menu->target != 1 ? '' : 'target="_blank"'}}>
                                            @elseif($menu->status == 4)
                                            <a data-hover="{{$menu->title}}" href="{{url('page/'.$menu->link)}}" {{$menu->target != 1 ? '' : 'target="_blank"'}}>
                                            @endif
                                                @if(\App\Menu::where('parent_id',$menu->id)->count())
                                                    <i class="fa fa-folder-open"></i>
                                                @else
                                                    <i class="fa fa-file-text"></i>
                                                @endif
                                                {{$menu->title}}
                                            </a>
                                            @if(\App\Menu::where('parent_id',$menu->id)->count())
                                            <ul class="drop-menu-div list-unstyled">
                                                @foreach(\App\Menu::where('parent_id',$menu->id)->orderBy('order_by','asc')->get() as $subMenu)
                                                    <li>
                                                        @if($subMenu->status == 1)
                                                        <a href="http://{{$subMenu->link}}" {{$subMenu->target != 1 ? '' : 'target="_blank"'}}>{{$subMenu->title}}</a>
                                                        @elseif($subMenu->status == 2)
                                                        <a href="#!">{{$subMenu->title}}</a>
                                                        @elseif($subMenu->status == 3)
                                                        <a href="{{url('accounts/search?platform_id[]='.$subMenu->link)}}" {{$subMenu->target != 1 ? '' : 'target="_blank"'}}>{{$subMenu->title}}</a>
                                                        @elseif($subMenu->status == 4)
                                                        <a href="{{url('page/'.$subMenu->link)}}" {{$subMenu->target != 1 ? '' : 'target="_blank"'}}>{{$subMenu->title}}</a>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                    <li><a href="{{url('/register')}}" data-hover="إشترك الان"><i class="fa fa-users"></i>إشترك الان</a></li>
                                    <li><a href="{{url('contact')}}" data-hover="إتصل بنا"><i class="fa fa-phone"></i>إتصل بنا</a></li>

                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!--end side-menu-->
                </div>
                <!--end user-grid-->
            </div>
        </div>
    </header>
             </div>
    <!--end header-->
