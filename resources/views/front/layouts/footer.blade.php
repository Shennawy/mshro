<!--start footer
             ================-->

    <footer>
        <div class="container">
            <div class="row">
                <!--start footer-menu-grid-->
                <div class="col-xl-3 col-sm-6 footer-menu-grid ">
                    <h2 class="sec-head wow fadeIn"><span>من نحن</span></h2>
                    <div class="footer-menu wow fadeIn">
                        <ul class="list-unstyled">
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end footer-menu-grid-->

                <!--start footer-menu-grid-->
                <div class="col-xl-3 col-sm-6 footer-menu-grid">
                    <h2 class="sec-head wow fadeIn"><span>مشاريع</span></h2>
                    <div class="footer-menu wow fadeIn">
                        <ul class="list-unstyled">
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end footer-menu-grid-->


                <!--start footer-menu-grid-->
                <div class="col-xl-3 col-sm-6 footer-menu-grid">
                    <h2 class="sec-head wow fadeIn"><span>روابط سريعه</span></h2>
                    <div class="footer-menu wow fadeIn">
                        <ul class="list-unstyled">
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                            <li>
                                <a href="#" data-hover="روابط نصية">روابط نصية</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end footer-menu-grid-->



                <!--start footer-social-->
                <div class="col-xl-3 col-sm-6 footer-social">
                    <h2 class="sec-head wow fadeIn"><span>تابع موقعنا على</span> </h2>
                    <div class="social-icons wow fadeIn">
                        <span class="follow-text wow fadeIn">تابعنا على</span>
                        <ul class="list-inline">
                        @foreach(\App\SiteSetting::where('module','social')->where('type',30)->get() as $social)
                            @if(getSettings($social->name.'Act'))
                            <li class="social-container {{$social->name}}-icon">
                                <a href="{{$social->value}}" target="_blank">
                                    <div class="social-cube">
                                        <div class="front">
                                            <i class="fa fa-{{$social->name}}"></i>
                                        </div>
                                        <div class="bottom">
                                            <i class="fa fa-{{$social->name}}"></i>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            @endif
                        @endforeach
                        </ul>
                    </div>

                    <a href="#" class="bank wow fadeIn"><img src="/front/images/main/pay1.png" alt="pay"></a>
                    <a href="#" class="bank wow fadeIn"><img src="/front/images/main/pay2.png" alt="pay"></a>
                </div>
            </div>
            <!--end footer-social-->
        </div>
        </div>
    </footer>
    <!--end footer-->

    <div class="copyrights text-center">
        <div class="container">
            <div class="row">
                <a href="#" class="col-12"><i class="fa fa-copyright"></i>{{getSettings('copyright-ar')}}</a>
            </div>
        </div>
    </div>
