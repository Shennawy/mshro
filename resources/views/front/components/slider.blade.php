
    <!--start slider
             ================-->

    <section class="slider">
        <div class="container-fluid">
            <div class="row">
                <div id="owl-demo" class="owl-carousel owl-theme first-owl main-owl">
                    @foreach(\App\slider::all() as $slider)
                    <!-- start owl-item -->
                    <div class="item">
                        <div class="slider-img full-width-img">
                            <img src="{{Request::root()}}/uploads/sliders/{{$slider->image}}" alt="{{$slider->title}}" class="converted-img" />
                        </div>
                    </div>
                    <!-- end owl-item -->
                    @endforeach

                </div>
            </div>

        </div>
    </section>
    <!--end slider-->
