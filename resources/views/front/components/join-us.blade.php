<!--start join_us
           ================-->

  <section class="join_us text-center">
      <div class="container">
          <div class="row">
              <div class="join-div col-12">
                  <h2 id="typing-text" class="wow fadeIn">هل انت جاهز لبداية مشروعك الخاص !</h2>
                  <a class="blue_btn custom_btn wow fadeInUp" href="join.html">ابتدى معنا</a>
              </div>
          </div>
      </div>
  </section>

  <!--end join_us-->
