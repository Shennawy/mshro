
    <!--start about
             ================-->

    <section class="about marg-sec">
        <div class="container">
            <div class="row">
                <!--start about-text-grid-->
                <div class="col-lg-6 col-md-12 about-text-grid text-right">
                    <h2 class="col-12 sec-head wow fadeInDown"><span>{{getSettings('main1')}}</span></h2>
                    <p class="col-12 dark-prg about-prg wow fadeIn">{{getSettings('sub1')}}</p>

                    <!--start icons-div-->
                    <div class="col-12 icons-div">
                        <div class="icon-div wow fadeIn">
                            <div class="icon-img"></div>
                            <div class="icon-text">
                                <h3 class="icon-title">{{getSettings('main2')}}</h3>
                                <p class="icon-prg">{{getSettings('sub2')}}</p>
                            </div>
                        </div>
                        <div class="icon-div wow fadeIn">
                            <div class="icon-img"></div>
                            <div class="icon-text">
                                <h3 class="icon-title">{{getSettings('main3')}}</h3>
                                <p class="icon-prg">{{getSettings('sub3')}}</p>
                            </div>
                        </div>
                        <div class="icon-div wow fadeIn">
                            <div class="icon-img"></div>
                            <div class="icon-text">
                                <h3 class="icon-title">{{getSettings('main4')}}</h3>
                                <p class="icon-prg">{{getSettings('sub4')}}</p>
                            </div>
                        </div>

                    </div>
                    <!--end icons-div-->
                </div>
                <!--end about-text-grid-->

                <!--start vedio-grid-->
                <div class="col-lg-6 col-md-12  vedio-grid">
                    <div class="main-vedio wow fadeIn">
                            <div class="full-width-img">

                                    <iframe width="100%" height="100%" id="main-vedio-div" src="{{getSettings('indexVideo')}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                <div class="vd-img"></div>
                            </div>
                    </div>
                </div>
                <!--end vedio-grid-->

            </div>
        </div>
    </section>

    <!--end about-->
