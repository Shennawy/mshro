
    <!--start webiste-sections
             ================-->

    <section class="webiste-sections text-center marg-sec">
        <div class="container">
            <div class="row">
                <h2 class="col-12 sec-head center-sec wow fadeIn"><span>أقسام الموقع</span></h2>
                <!--start web-sec-div-->
                <div class="web-sec-div col-lg-3 col-sm-6 ">
                    <div class="web-sec wow fadeIn">
                        <div class="web-sec-icon">
                            <i class="fa fa-print"></i>
                        </div>
                        <h3 class="web-sec-title">مطبوعات و دور نشر</h3>
                        <p class="web-sec-prg">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</p>
                        <a href="website_sec.html" class="more_link"><span data-hover="المزيد عن القسم">المزيد عن القسم</span></a>
                    </div>
                </div>
                <!--end web-sec-div-->

                <!--start web-sec-div-->
                <div class="web-sec-div col-lg-3 col-sm-6 ">
                    <div class="web-sec wow fadeIn">
                        <div class="web-sec-icon">
                            <i class="fa fa-book"></i>
                        </div>
                        <h3 class="web-sec-title">صفقات بيع كتب</h3>
                        <p class="web-sec-prg">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</p>
                        <a href="website_sec.html" class="more_link"><span data-hover="المزيد عن القسم">المزيد عن القسم</span></a>
                    </div>
                </div>
                <!--end web-sec-div-->


                <!--start web-sec-div-->
                <div class="web-sec-div col-lg-3 col-sm-6 ">
                    <div class="web-sec wow fadeIn">
                        <div class="web-sec-icon">
                            <i class="fa fa-paint-brush"></i>
                        </div>
                        <h3 class="web-sec-title">التصميم بانواعه</h3>
                        <p class="web-sec-prg">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</p>
                        <a href="website_sec.html" class="more_link"><span data-hover="المزيد عن القسم">المزيد عن القسم</span></a>
                    </div>
                </div>
                <!--end web-sec-div-->


                <!--start web-sec-div-->
                <div class="web-sec-div col-lg-3 col-sm-6 ">
                    <div class="web-sec wow fadeIn">
                        <div class="web-sec-icon">
                            <i class="fa fa-desktop"></i>
                        </div>
                        <h3 class="web-sec-title">طلبات الاعلانات</h3>
                        <p class="web-sec-prg">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</p>
                        <a href="website_sec.html" class="more_link"><span data-hover="المزيد عن القسم">المزيد عن القسم</span></a>
                    </div>
                </div>
                <!--end web-sec-div-->


            </div>
        </div>
    </section>

    <!--end webiste-sections-->
