
    <!--start services
          ================-->

    <section class="services text-right marg-sec">
        <div class="container">
            <div class="row">
                <!--start service-grid-list-->
                <div class="col-lg-4 col-md-12 service-grid-list">
                    <h2 class="sec-head wow fadeInDown"><span>{{getSettings('siteServ-title-r')}}</span></h2>
                    <!--start service-div-list-->

                    <div class="service-div-list wow fadeIn">
                        <ul class="list-unstyled">
                            <li><span>{{getSettings('siteServ-str-r1')}}</span></li>
                            <li><span>{{getSettings('siteServ-str-r2')}}</span></li>
                            <li><span>{{getSettings('siteServ-str-r3')}}</span></li>
                            <li><span>{{getSettings('siteServ-str-r4')}}</span></li>
                            <li> <span>{{getSettings('siteServ-str-r5')}}</span></li>
                        </ul>
                    </div>
                    <!--end service-div-list-->

                    <!--start advert-img-->
                    <a href="#" class="advert-link">
                    <div class="advert-img adv-div full-width-img wow fadeIn">
                        <img src="{{getSettings('siteAds-ad-r')}}" alt="إعلان" class="converted-img" />
                    </div>
                    </a>
                    <!--start advert-img-->
                </div>
                <!--end service-grid-list-->

                <!--start service-grid-text-->
                <div class="col-lg-8 col-md-12 service-grid-text">
                    <div class="services-text">
                        <div class="rocket"><i class="fa fa-rocket"></i></div>
                        <h2 class="col-12 sec-head wow fadeInDown"><span>{{getSettings('siteServ-title-l')}}</span> </h2>
                        <p class="col-12 wow fadeIn">{{getSettings('siteServ-str-l')}}</p>
                        <!--start service-div-icons-->
                        <div class="service-div-icons text-center row">
                            <!--start serv-icon-->
                            <div class="serv-icon col-lg-4 col-6 wow fadeIn">
                                <div class="serv-icon-img"><img src="/front/images/icons/4.png" alt="icon" /></div>
                                <span class="stat-number">{{getSettings('siteServ-num-l1')}}</span>
                                <h3 class="serv-icon-title">صفقات اليوم المنتهيه</h3>
                            </div>
                            <!--end serv-icon-->

                            <!--start serv-icon-->
                            <div class="serv-icon col-lg-4 col-6 wow fadeIn">
                                <div class="serv-icon-img"><img src="/front/images/icons/5.png" alt="icon" /></div>
                                <span class="stat-number">{{getSettings('siteServ-num-l2')}}</span>
                                <h3 class="serv-icon-title">صفقات الاسبوع المنتهيه</h3>
                            </div>
                            <!--end serv-icon-->

                            <!--start serv-icon-->
                            <div class="serv-icon col-lg-4 col-6 wow fadeIn">
                                <div class="serv-icon-img"><img src="/front/images/icons/6.png" alt="icon" /></div>
                                <span class="stat-number">{{getSettings('siteServ-num-l3')}}</span>
                                <h3 class="serv-icon-title">صفقات الشهر المنتهيه</h3>
                            </div>
                            <!--end serv-icon-->

                        </div>
                        <!--start service-div-icons-->

                    </div>
                </div>
                <!--end service-grid-text-->

            </div>
        </div>
    </section>

    <!--end services-->
