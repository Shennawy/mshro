@extends('front.layouts.master')
@section('title','التدوينات | ')
@section('header')
    {!! Html::style('front/css/jPages.min.css') !!}
@endsection
@section('content')
    <!--start pages-head ================-->
    <section class="pages-head">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="white-title sec-head center-sec wow fadeIn"><span>قسم التدوينات</span></h1>

                </div>
            </div>
        </div>
    </section>
    <!--end pages-head-->


    <!--start  projects-done ================-->

    <section class="personal-file projects-done text-right marg-sec">
        <div class="container">
            <div class="row">
                <!--start blog-sec-->
                <div class="blog-sec col-lg-12">
                    <div class="row" id="itemcontainer2">
                        @foreach($blogs as $blog)
                        <!--start blog-div-grid-->
                        <div class="col-lg-4 col-md-6 project-done-grid blog-div-grid wow fadeIn">
                            <div class="project-done-div">
                                <a href="{{Request::root()}}/{{$blog->image}}" data-group="set1" class="html5lightbox">
                                    <div class="project-done-img full-width-img">
                                        <img src="{{Request::root()}}/{{$blog->image}}" alt="img" class="converted-img"/>
                                    </div>
                                </a>
                                <div class="project-done-text">
                                    <div class="tag-lists blog-tag-list text-center wow fadeIn">
                                        <ul class="list-inline">
                                            <li><a href="#"><i class="fa fa-user"></i>{{$blog->User->name}}</a></li>
                                            {{--<li><a href="#"><i class="fa fa-comment"></i> 20 تعليق </a></li>--}}
                                        </ul>
                                    </div>
                                    <a class="blog-title" href="{{url('blog/'.$blog->id)}}">{{$blog->title}} </a>
                                    <a class="blue_btn custom_btn wow fadeInUp" href="{{url('blog/'.$blog->id)}}">اقرأ المزيد</a>

                                </div>
                            </div>
                        </div>
                        <!--end blog-div-grid-->
                        @endforeach
                    </div>
                    <div class="holder_pager holder2"></div>

                </div>
                <!--end blog-sec-->

            </div>


        </div>
    </section>

    <!--end  projects-done-->
    @include('front.components.join-us')

@endsection
@section('footer')
@endsection