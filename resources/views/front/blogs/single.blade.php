@extends('front.layouts.master')
@section('title','التدوينات | ')
@section('header')
    {!! Html::style('front/css/jPages.min.css') !!}
@endsection
@section('content')
    <!--start pages-head
          ================-->
    <section class="pages-head">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="white-title sec-head center-sec wow fadeIn"><span>{{$blog->title}}</span></h1>

                </div>
            </div>
        </div>
    </section>
    <!--end pages-head-->

    <!--start  projects-done ================-->
    <section class="personal-file blog-details-sec text-right marg-sec">
        <div class="container">
            <div class="row">
                <!--start blog-sec-->
                <div class="blog-sec col-lg-12">
                    <!--start main-chat-grid-->
                    <div class="main-view-grid">
                        <div class="main-view-div">
                            <div class="inner-view-div">
                                <h3 class="dark_title wow fadeIn">{{$blog->title}}</h3>
                                <div class="tag-lists blog-tag-list  wow fadeIn">
                                    <ul class="list-inline">
                                        <li><a href="#"><i class="fa fa-user"></i>{{$blog->User->name}}</a></li>
                                    </ul>
                                </div>
                                <!--start view-project-details-->
                                <div class="view-project-details">
                                    <p class="blog-prg wow fadeIn">
                                        {!! $blog->body !!}
                                    </p>
                                </div>
                                <!--end view-project-details-->
                            </div>
                        </div>
                    </div>
                    <!--end login-form-grid-->
                </div>
                <!--end blog-sec-->
            </div>
        </div>
    </section>

    <!--end  projects-done-->
    @include('front.components.join-us')

@endsection
@section('footer')
@endsection