@extends('front.layouts.master')
@section('title',\App\Page::where('pLink',$pageName)->first()->pName.' | ')
@section('header')
@endsection
@section('content')
    @if(isset($new))

    <!--start pages-head ================-->
    <section class="pages-head">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="white-title sec-head center-sec wow fadeIn"><span> {{$new->title}} </span></h1>
                    <p class="white-prg wow fadeIn">{!! str_limit($new->body, 120) !!}</p>
                </div>
            </div>
        </div>
    </section>
    <!--end pages-head-->

    <!--start login-pg ================-->
    <section class="services login-pg text-right marg-sec">
        <div class="container">
            <div class="row">
                <!--start typography-sec-->
                <div class="col-12  typography-sec wow fadeIn">
                    <h2>{{$new->title}}
                    </h2>
                    <p>{!! $new->body !!}</p>
                </div>
                <!--end typography-sec-->

            </div>
        </div>
    </section>
    <!--end login-pg-->
    @elseif(\App\Page::where('pLink',$pageName)->count())
        <!--start pages-head ================-->
        <section class="pages-head">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1 class="white-title sec-head center-sec wow fadeIn"><span> {{ \App\Page::where('pLink',$pageName)->first()->pName }} </span></h1>
                    </div>
                </div>
            </div>
        </section>
        <!--end pages-head-->

        <!--start login-pg ================-->
        <section class="services login-pg text-right marg-sec">
            <div class="container">
                <div class="row">
                    <!--start typography-sec-->
                    <div class="col-12  typography-sec wow fadeIn">
                        <h2>{{ \App\Page::where('pLink',$pageName)->first()->pName }}
                        </h2>
                        <p>{!! \App\Page::where('pLink',$pageName)->first()->content !!}</p>
                    </div>
                    <!--end typography-sec-->

                </div>
            </div>
        </section>
        <!--end login-pg-->

    @endif
        @include('front.components.join-us')

@endsection
@section('footer')
@endsection