@extends('front.layouts.master')

@section('content')
  <!--start login-pg
          ================-->

    <section class="services login-pg text-right marg-sec">
        <div class="container">
            <div class="row">

                <!--start login-form-grid-->
                <div class="col-lg-8 col-md-12 login-form-grid">
                    <div class="login-form-div add-project-form">
                        <form class="needs-validation row" novalidate action="{{url("posts/save")}}" method="post">
                          {{ csrf_field() }}
                            <div class="form-group  col-12 wow fadeIn">
                                <label>عنوان المشروع <span>*</span></label>
                                <input type="text" name="title" class="form-control" required>
                                <span class="project-input-note">أدرج عنوانا موجزا يصف مشروعك بشكل دقيق. </span>
                                <div class="invalid-feedback">
                                    من فضلك أدخل عنوان المشروع
                                </div>
                            </div>


                            {{-- <div class="form-group  col-12 wow fadeIn">
                                <label>رابط للمشروع <span>*</span></label>
                                <input type="text" name="url" class="form-control" required>
                                <span class="project-input-note">أدرج عنوانا موجزا يصف مشروعك بشكل دقيق. </span>
                                <div class="invalid-feedback">
                                    من فضلك أدخل رابط المشروع
                                </div>
                            </div> --}}


                           <div class="form-group  col-12 wow fadeIn">
                                <label>المهارات المطلوبة لتنفيذ المشروع<span>*</span></label>
                                <input type="text" name="tags" class="form-control" required>
                                <span class="project-input-note">حدد أهم المهارات المطلوبة لتنفيذ مشروعك. </span>
                                <div class="invalid-feedback">
                                    من فضلك أدخل المهارات المطلوبة
                                </div>
                            </div>

                            <div class="form-group  col-12 wow fadeIn">
                                <label>تفاصيل المشروع <span>*</span></label>
                                <textarea class="form-control" name="desc" required></textarea>
                                <span class="project-input-note">أدرج وصفا مفصّلا ودقيقا لمشروعك. </span>
                                <div class="invalid-feedback">
                                    من فضلك أدخل تفاصيل المشروع
                                </div>
                            </div>




                            <div class="form-group  col-md-6 wow fadeIn">
                                <label>الميزانية المتوقعة<span>*</span></label>
                                <input type="text" name="price" class="form-control" required>

                                <span class="project-input-note">اختر ميزانية مناسبة لتحصل على عروض جيدة </span>
                                <div class="invalid-feedback">
                                    من فضلك أدخل الميزانية المتوقعة
                                </div>
                            </div>

                            <div class="form-group  col-md-6 wow fadeIn">
                                <label>المدة المتوقعة للتسليم (بالأيام)<span>*</span></label>
                                <input type="text" name="duration" class="form-control" required>
                                <span class="project-input-note">متى تحتاج استلام مشروعك </span>
                                <div class="invalid-feedback">
                                    من فضلك أدخل المدة المتوقعة للتسليم
                                </div>
                            </div>

                            <div class="form-group  col-12 wow fadeIn">
                                <label>ملفات توضيحية</label>
                                <input type="file" name="image" accept=".xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf" multiple>
                            </div>




                            <div class="form-group submit-form-group two-btns col-12 wow fadeIn">
                                <button type="submit" class="blue_btn custom_btn">نشر المشروع</button>
                                {{-- <button type="submit" class="blue_btn no-bord-btn custom_btn">حفظ كمسودة</button> --}}

                            </div>




                        </form>
                    </div>
                </div>
                <!--end login-form-grid-->
                <!--start service-grid-list-->
                <div class="col-lg-4 col-md-12 service-grid-list wow fadeIn">
                    <!--start project-about-->
                    <div class="project-about wow fadeInUp">
                        <h3 class="blue-head">ابدأ ببناء مشروعك</h3>
                        <p class="dark-prg project-about-prg">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ</p>
                    </div>
                    <!--end project-about-->

                    <!--start project-about-->
                    <div class="project-about wow fadeInUp">
                        <h3 class="blue-head">ابدأ ببناء مشروعك</h3>
                        <p class="dark-prg project-about-prg">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ</p>
                    </div>
                    <!--end project-about-->


                </div>
                <!--end service-grid-list-->


            </div>
        </div>
    </section>

    <!--end login-pg-->


@endsection
