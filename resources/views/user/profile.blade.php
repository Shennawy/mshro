@extends('front.layouts.master')

@section('content')
  <!--start pages-head
          ================-->
    <section class="pages-head">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="white-title sec-head center-sec wow fadeIn"><span>{{$user->name}}</span></h1>
                    <p class="white-prg wow fadeIn">{{$user->head_line}}</p>
                    {{-- <a class="blue_btn custom_btn wow fadeInUp" href="#">متابعة العضو</a> --}}
                </div>
            </div>
        </div>
    </section>
    <!--end pages-head-->

    <!--start personal-file
          ================-->

    <section class="personal-file text-right marg-sec">
        <div class="container">
            <div class="row">
                <!--start prof-links-->
                <div class="col-12">
                    <div class="prof-main-links">
                      @if(Auth::user() && $user->id == Auth::user()->id)
                        <a href="{{url('/messages')}}" class="wow fadeInDown">
                            <div class="prof-link-div">
                                <span class="main-icon-tab"><i class="fa fa-envelope-open-o"></i></span>
                                <span class="prof-link-title"><i class="fa fa-caret-left"></i> الرسائـــــــل</span>
                                الرسائل الغير مقروئه : {{$user->unreadMessagesCount()}}
                            </div>
                        </a>
                        <a href="projects.html" class="wow fadeInDown">
                            <div class="prof-link-div">
                                <span class="main-icon-tab"><i class="fa fa-check-circle-o"></i></span>
                                <span class="prof-link-title"><i class="fa fa-caret-left"></i> عدد الصفقات</span>
                                <ul class="list-inline">
                                    <li>المنتهيه : 1 </li>
                                    <li>المعلقه : 1 </li>
                                    <li>قيد الانتظار : 1 </li>
                                </ul>
                            </div>
                        </a>
                        <a href="followers.html" class="wow fadeInDown">
                            <div class="prof-link-div">
                                <span class="main-icon-tab"><i class="fa fa-users"></i></span>
                                <span class="prof-link-title"><i class="fa fa-caret-left"></i> المتابعين</span>
                            </div>
                        </a>
                        <a href="following.html" class="wow fadeInDown">
                            <div class="prof-link-div">
                                <span class="main-icon-tab"><i class="fa fa-user-plus"></i></span>
                                <span class="prof-link-title"><i class="fa fa-caret-left"></i> المتابعون</span>
                            </div>
                        </a>
                      @else
                        <a href="{{url('messages/user/'.$user->id)}}" class="wow fadeInDown">
                            <div class="prof-link-div">
                                <span class="main-icon-tab"><i class="fa fa-envelope-open-o"></i></span>
                                <span class="prof-link-title"><i class="fa fa-caret-left"></i> الرسائـــــــل</span>
                              ارسال رسالة
                            </div>
                        </a>
                        <a href="projects.html" class="wow fadeInDown">
                            <div class="prof-link-div">
                                <span class="main-icon-tab"><i class="fa fa-check-circle-o"></i></span>
                                <span class="prof-link-title"><i class="fa fa-caret-left"></i> عدد الصفقات</span>
                                <ul class="list-inline">
                                    <li>المنتهيه : 1 </li>
                                    <li>المعلقه : 1 </li>
                                    <li>قيد الانتظار : 1 </li>
                                </ul>
                            </div>
                        </a>

                      @endif
                    </div>
                </div>
                <!--end prof-links-->

              @include('user.personal-grid')

                <!--start prof-projct-grid-->
                <div class="col-lg-8 prof-projct-grid">
                    <!--start prof-summry-->
                    <div class="prof-summry wow fadeIn">
                        <h3 class="inner-section-title">نبذه عني !! </h3>
                        <p>{{$user->Profile->bio}}</p>
                    </div>
                    <!--end prof-summry-->

                    <!--start Experiences-->
                    <div class="experiences wow fadeIn">
                        <ul class="list-unstyled">
                            <li>{{$user->Profile->experience}} </li>
                            </ul>
                    </div>
                    <!--end Experiences-->


                    <!--start projects-done-->
                    <div class="projects-done">
                        <div class="row">
                            <h2 class="sec-head col-12"><span>مشاريع تم تنفيذها</span></h2>

                            @foreach ($user->Works as $key => $work)
                              @include('user.profile-single-work')
                            @endforeach
                            @if(Auth::user() && Auth::user()->id == $user->id)

                              <div class="col-12">
                                  <a class="blue_btn custom_btn wow fadeIn" href="{{url("users/$user->id/works/add")}}">اضافة مشروع</a>
                                </div>
                            @endif
                            <div class="col-12">
                                <a class="blue_btn custom_btn wow fadeIn" href="{{url("users/$user->id/works")}}">المزيد من المشروعات</a>
                              </div>
                        </div>
                    </div>
                    <!--end projects-done-->

                    <div class="search-words wow fadeIn">
                        <h2 class="sec-head"><span>كلمات البحث</span></h2>
                        <a href="#">دلالية</a>
                        <a href="#">دلالية</a>
                        <a href="#">دلالية</a>
                        <a href="#">دلالية</a>
                        <a href="#">دلالية</a>
                        <a href="#">دلالية</a>
                        <a href="#">دلالية</a>
                        <a href="#">دلالية</a>
                        <a href="#">دلالية</a>
                        <a href="#">دلالية</a>
                    </div>
                </div>
                <!--end prof-projct-grid-->

            </div>
        </div>
    </section>

    <!--end personal-file-->

@endsection
