<!--start personal-user-grid-->

<div class="col-lg-4 personal-user-grid wow fadeIn">
    <div class="personal-info">
        <h3 class="p-user">
            <i class="fa fa-circle"></i>
            {{$user->name}}
            @if(auth()->user() && auth()->user()->id == $user->id)
            <a href="{{url('edit-profile')}}" class="edit-div"><i class="fa fa-pencil"></i></a>
          @endif
        </h3>

        <div class="p-right-list">
            <ul class="list-unstyled">
                {{-- <li>
                    <span><i class="fa fa-star-half-o"></i> التقييم العام : </span>
                    <div class="static-stars inline-stars">
                        <input class="star" id="s1" type="radio" name="star" />
                        <label class="star" for="s1"></label>
                        <input class="star" id="s2" type="radio" name="star" />
                        <label class="star" for="s2"></label>
                        <input class="star" id="s3" type="radio" name="star" />
                        <label class="star" for="s3"></label>
                        <input class="star" id="s4" type="radio" name="star" />
                        <label class="star" for="s4"></label>
                        <input class="star" id="s5" type="radio" name="star" />
                        <label class="star" for="s5"></label>
                    </div>

                    2.3
                </li> --}}
                <li>
                    <span><i class="fa fa-graduation-cap"></i> المسمى الوظيفي : </span>
                    {{$user->Profile->head_line}}

                </li>
                <li>
                    <span><i class="fa fa-cog"></i> مشاريع تم تنفيذها : </span>
                    15 مشروع
                </li>
                {{-- <li>
                    <span><i class="fa fa-map-marker"></i> المدينة : </span>
                    الباحة
                </li>
                <li>
                    <span><i class="fa fa-calendar"></i> العمر : </span>
                    22 عاما
                </li> --}}
            </ul>
        </div>

        <div class="statistics wow fadeIn">
            <h2 class="sec-head"><span>إحصائيات</span></h2>
            <ul class="list-unstyled">
                <li><a href="projects.html"><i class="fa fa-plus-square"></i>مشاريع تم تنفيذها</a></li>
                {{-- <li><a href="projects.html"><i class="fa fa-envelope"></i>مشاركات العضو</a></li> --}}
                {{-- <li><a href="projects.html"><i class="fa fa-caret-left"></i>نسبة ظهور العضو</a></li> --}}
                {{-- <li><a href="projects.html"><i class="fa fa-caret-left"></i>التقييمات</a></li> --}}
            </ul>
        </div>

        <div class="skills wow fadeIn">
            <h2 class="sec-head"><span>المهارات</span></h2>
            @foreach (explode(',', $user->Profile->skills) as $key => $skill)
              <div class="progress">
                  <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">{{$skill}}</div>
              </div>
            @endforeach
        </div>
    </div>
</div>
<!--end personal-user-grid-->
