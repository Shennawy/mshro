@extends('front.layouts.master')


@section('content')

  @php
    $user = Auth::user();
  @endphp
  <!--start login-pg
          ================-->

    <section class="services login-pg text-right marg-sec">
        <div class="container">
            <div class="row">

                <!--start login-form-grid-->
                <div class="col-lg-8 col-md-12 login-form-grid">
                    <div class="login-form-div add-project-form">
                        <form class="needs-validation row" novalidate action="" method="post">
                            <div class="form-group  col-12 wow fadeIn">
                                <label>الاسم</label>
                                <input type="text" class="form-control" value="{{$user->name}}" required>
                                <span class="project-input-note">اسمك</span>
                                <div class="invalid-feedback">
                                    من فضلك أدخل اسمك
                                </div>
                            </div>
                            <div class="form-group  col-12 wow fadeIn">
                                <label>المسمى الوظيفي</label>
                                <input type="text" class="form-control" value="{{$user->Profile->head_line}}" required>
                                <span class="project-input-note">المسمى الوظيفي</span>
                                <div class="invalid-feedback">
                                    من فضلك أدخل المسمى الوظيفي
                                </div>
                            </div>
                            <div class="form-group  col-12 wow fadeIn">
                                <label>اسم المستخدم</label>
                                <input type="text" class="form-control" value="{{$user->Profile->username}}" required>
                                <span class="project-input-note">اسم المستخدم</span>
                                <div class="invalid-feedback">
                                    من فضلك أدخل اسم المستخدم
                                </div>
                            </div>

                            <div class="form-group  col-12 wow fadeIn">
                                <label>نبذة </label>
                                <textarea class="form-control" name="bio" required>{{$user->Profile->bio}}</textarea>
                                <span class="project-input-note">أدرج نبذة مختصرة عنك</span>
                                <div class="invalid-feedback">
                                    من فضلك أدخل نبذة مختصرة
                                </div>
                            </div>


                            <div class="form-group  col-12 wow fadeIn">
                                <label>المهارات</label>
                                <textarea class="form-control" name="skills" required>{{$user->Profile->skills}}</textarea>
                                <span class="project-input-note">أدرج مهاراتك</span>
                                <div class="invalid-feedback">
                                    من فضلك أدخل مهاراتك
                                </div>
                            </div>









                            <div class="form-group submit-form-group two-btns col-12 wow fadeIn">
                                <button type="submit" class="blue_btn custom_btn">حفظ</button>

                            </div>




                        </form>
                    </div>
                </div>
                <!--end login-form-grid-->
                <!--start service-grid-list-->
                <div class="col-lg-4 col-md-12 service-grid-list wow fadeIn">
                    <!--start project-about-->
                    <div class="project-about wow fadeInUp">
                        <h3 class="blue-head">ابدأ ببناء مشروعك</h3>
                        <p class="dark-prg project-about-prg">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ</p>
                    </div>
                    <!--end project-about-->

                    <!--start project-about-->
                    <div class="project-about wow fadeInUp">
                        <h3 class="blue-head">ابدأ ببناء مشروعك</h3>
                        <p class="dark-prg project-about-prg">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ</p>
                    </div>
                    <!--end project-about-->


                </div>
                <!--end service-grid-list-->


            </div>
        </div>
    </section>

    <!--end login-pg-->


@endsection
