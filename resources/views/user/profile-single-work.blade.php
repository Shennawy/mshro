
                            <!--start project-done-grid-->
                            <div class="col-sm-6 project-done-grid wow fadeIn">
                                <div class="project-done-div">
                                    <a href="{{$work->image}}" data-group="set1" class="html5lightbox">
                                        <div class="project-done-img full-width-img" style="background-image:url({{$work->image}})">
                                            <img src="{{$work->image}}" alt="img" class="converted-img" />]
                                        </div>
                                    </a>
                                    <div class="project-done-text">
                                        <h3>{{$work->tite}}</h3>
                                        <p>{{$work->desc}}</p>
                                        <a href="{{$work->url}}" class="more_link no-marg-more"><span data-hover="تصفح المشروع">تصفح المشروع</span></a>

                                    </div>
                                </div>
                            </div>
                            <!--end project-done-grid-->
