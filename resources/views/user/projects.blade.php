@extends('front.layouts.master')

@section('content')
<!--start pages-head
        ================-->
  <section class="pages-head">
      <div class="container">
          <div class="row">
              <div class="col-12">
                  <h1 class="white-title sec-head center-sec wow fadeIn"><span>مشاريع تم تنفيذها بواسطة العضو : {{$user->name}}</span></h1>
                  <p class="white-prg wow fadeIn">{{$user->Profile->head_line}}</p>
                  {{-- <a class="blue_btn custom_btn wow fadeInUp" href="#">متابعة العضو</a> --}}
              </div>
          </div>
      </div>
  </section>
  <!--end pages-head-->
  <!--start  projects-done
      ================-->

<section class="personal-file projects-done text-right marg-sec">
    <div class="container">
        <div class="row">
            <h2 class="sec-head col-12"><span>مشاريع تم تنفيذها</span></h2>

            @foreach ($user->Works as $key => $work)
              @include('user.single-project')
            @endforeach

            <div class="col-12">
                <a class="blue_btn custom_btn wow fadeIn" href="personal-file.html">العوده لصفحة البروفايل <i class="fa fa-arrow-left"></i></a>

                <div class="search-words wow fadeIn">
                    <h2 class="sec-head"><span>كلمات البحث</span></h2>
                    <a href="#">دلالية</a>
                    <a href="#">دلالية</a>
                    <a href="#">دلالية</a>
                    <a href="#">دلالية</a>
                    <a href="#">دلالية</a>
                    <a href="#">دلالية</a>
                    <a href="#">دلالية</a>
                    <a href="#">دلالية</a>
                    <a href="#">دلالية</a>
                    <a href="#">دلالية</a>
                </div>
            </div>
        </div>
        <!--end prof-projct-grid-->

    </div>
    </div>
</section>

<!--end  projects-done-->

@endsection
