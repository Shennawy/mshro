@extends('front.layouts.master')

@section('content')
  @php
  if(auth()->user() && !auth()->user()->Profile){
    Auth::user()->createProfile();
    // dd(Auth::user()->Profile);
  }
  @endphp
  @include('front.components.slider')
  @include('front.components.about')
  @include('front.components.services')
  @include('front.components.join-us')
  @include('front.components.website-sections')
  @include('front.components.index-adv')
@endsection
