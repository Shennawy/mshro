<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('logout',function(){
    Auth::logout();
    return redirect('/');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'messages', 'middleware' => ['auth']], function () {
  Route::get('/', 'MessagesController@getChatPage')->name('chat-page');
  Route::get('/user/{id}', 'MessagesController@getChatPage');
  Route::post('/send', 'MessagesController@send');

});



// Blogs
Route::get('blogs', 'BlogController@viewAll');
Route::get('blog/{id}', 'BlogController@viewSingle');

// contact
Route::get('contact', 'ContactController@create');
Route::post('contact', 'ContactController@store');


// Pages
Route::get('page/{pageName}', function ($pageName) {
    if (view()->exists('site.pages.' . $pageName)) {
        return view('front.pages.' . $pageName);
    }

    if (\App\Page::where('pLink', $pageName)->count()) {
        return view('front.pages.page', compact('pageName'));
    }
    return \App::abort(404);
});

#=======================================================================================================================
#======================================= Auth -> Admin =================================================================
#=======================================================================================================================

// Admin Guard Authentication Routes...
Route::get('admincp/login', 'Admin\LoginController@showLoginForm')->name('admin.login');
Route::post('admincp/login', 'Admin\LoginController@login');
Route::get('admincp/password/reset', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('admincp/password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('admincp/password/reset/{token}', 'Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
Route::post('admincp/password/reset', 'Admin\ResetPasswordController@reset')->name('admin.password.update');

Route::group(['prefix' => 'admincp', 'middleware' => ['auth:admin']], function () {

    Route::get('logout', 'Admin\LoginController@logout')->name('admin.logout');

    // home.index
    Route::get('/', function () {
        return view('admin.home.index');
    });

    // Users => siteMembers
    Route::resource('users', 'UserController');
    Route::get('users/{id}/restore', 'UserController@restore');
    Route::get('users/{id}/force', 'UserController@force');

    // Admins Gaurd
    Route::resource('admins', 'AdminController');
    Route::get('admins/{id}/restore', 'AdminController@restore');
    Route::get('admins/{id}/force', 'AdminController@force');
    Route::get('change-password', 'AdminController@changePassword');
    Route::post('change-password', 'AdminController@changePassword');

    // ads
    Route::get('posts/{id}/', 'Admin\PostController@getPost');
    Route::get('posts/{id}/active', 'Admin\PostController@activatePost');
    Route::get('posts', 'Admin\PostController@getPosts');
    Route::post('posts/save', 'Admin\PostController@savePost');

    // Roles
    Route::resource('roles', 'RoleController');
    Route::get('roles/users/{roleName}', 'RoleController@UsersHasRole');

    // siteSettings
    Route::resource('settings', 'SettingController');

    // Blogs
    Route::resource('blogs', 'BlogController');

    // Sliders
    Route::resource('sliders', 'SliderController');

    // Pages
    Route::resource('pages', 'PageController');
    Route::post('pages/{id}/{active}', 'PageController@activate');

    // Contact
    Route::resource('contacts', 'ContactController');
    Route::post('contactsReplay', 'ContactController@replay');

    // Menus
    Route::resource('menus', 'MenuController');
    Route::post('menus/order', 'MenuController@order');
    Route::post('pages/{id}/{active}','PageController@activate');
    Route::get('pagesMenu/{id}','PageController@pagesMenu');

});

Route::get('/profile', function(){
  return redirect("/users/".Auth::user()->id);
});

Route::get('users/{profile}', 'ProfileController@index');

  Route::get('users/{profile}/works', 'ProfileController@getWorks');
  Route::get('edit-profile', 'ProfileController@editProf');
  Route::post('users/{profile}/edit', 'ProfileController@saveProf');

  Route::get('users/{profile}/works/add', 'ProfileController@addWork');
  Route::post('users/{profile}/works/save', 'ProfileController@saveWork');

  Route::get('users/{profile}/works/edit/{id}', 'ProfileController@editWork');
  Route::post('users/{profile}/works/remove', 'ProfileController@removeWork');

  Route::get('posts/new', 'PostController@add');
  Route::get('posts/add', 'PostController@add');

  Route::get('posts/edit/{id}', 'PostController@getEdit');
  Route::post('posts/save', 'PostController@save');
  Route::post('posts/offer', 'PostController@saveOffer');

  Route::get('posts/{id}', 'PostController@show');
