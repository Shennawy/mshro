<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSpecificationPostTable extends Migration {

	public function up()
	{
		Schema::create('specification_post', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('post_id')->unsigned();
			$table->integer('specification_id')->unsigned();
			$table->longText('value')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('specification_post');
	}
}