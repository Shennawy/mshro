<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 191)->nullable();
            $table->string('name', 255)->nullable();
            $table->mediumText('value')->nullable();
            $table->smallInteger('type')->nullable();
            $table->string('module', 191)->nullable();
            $table->smallInteger('orderBy')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
