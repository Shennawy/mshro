<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->morphs('owner');
            $table->string('extension')->nullable();
            $table->string('local_path');
            $table->integer('file_size');
            $table->string('mime')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('url');
            $table->boolean('is_active')->default(1);
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
