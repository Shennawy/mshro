<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSpecificationOptionTable extends Migration {

	public function up()
	{
		Schema::create('specification_option', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('specification_id')->unsigned();
			$table->string('name', 255)->nullable();
		});
	}

	public function down()
	{
		Schema::drop('specification_option');
	}
}