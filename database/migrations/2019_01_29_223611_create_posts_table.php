<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

	public function up()
	{
		Schema::create('posts', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('cat_id')->unsigned();
			$table->string('title', 255)->nullable();
			$table->text('desc')->nullable();
			$table->tinyInteger('type')->nullable()->default('1');
			$table->smallInteger('status')->default('0');
			$table->mediumText('tags')->nullable();
			$table->integer('price')->nullable();
			$table->tinyInteger('visibility')->default('1');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('posts');
	}
}