<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSpecificationsTable extends Migration {

	public function up()
	{
		Schema::create('specifications', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 255)->nullable();
			$table->smallInteger('type')->nullable();
			$table->integer('cat_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('specifications');
	}
}