<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfilesTable extends Migration {

	public function up()
	{
		Schema::create('profiles', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('age')->nullable();
			$table->text('address')->nullable();
			$table->text('skills')->nullable();
			$table->string('head_line', 255)->nullable();
			$table->text('bio')->nullable();
			$table->text('experience')->nullable();
			$table->string('image', 191)->nullable()->default('no_image.png');
			$table->longText('data')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('profiles');
	}
}