<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOffersTable extends Migration {

	public function up()
	{
		Schema::create('offers', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->text('body')->nullable();
			$table->integer('price')->nullable();
			$table->integer('duration')->nullable();
			$table->tinyInteger('status')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('offers');
	}
}
