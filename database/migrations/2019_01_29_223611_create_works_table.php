<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorksTable extends Migration {

	public function up()
	{
		Schema::create('works', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->string('title', 255)->nullable();
			$table->text('desc')->nullable();
			$table->string('image', 191)->nullable();
			$table->string('url', 255)->nullable();
			$table->text('tags')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('works');
	}
}