<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $models = ['role','user','admin','blog','page','menu'];
        $permissions = [
            '-list' => 'عرض ',
            '-create' => 'إضافة',
            '-edit' => 'التعديل ',
            '-delete' => 'حذف ',
        ];

        foreach ($models as $model) {
            foreach ($permissions as $key => $val) {
                if(!Permission::where('name',$model.$key)->count()){
                    Permission::create(['name' => $model.$key,'slug'=>$val,'orderBy'=>$model.'Model','type'=>'admin','guard_name'=>'admin']);
                    echo $model.$key."\n";
                }
            }
        }
//        when you create custom new permission edit & unComment next line ====> hash the line after implementation
//        Permission::create(['name' => 'setting-list','slug'=>'التحكم بإعدادات الموقع','orderBy'=>'settingModel','type'=>'admin','guard_name'=>'admin']);
//        Permission::create(['name' => 'contact-list','slug'=>'عرض','orderBy'=>'contactModel','type'=>'admin','guard_name'=>'admin']);
    }
}
